import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { tagWithCurrentStageIndex } from 'https://jslib.k6.io/k6-utils/1.3.0/index.js';
import { tagWithCurrentStageProfile } from 'https://jslib.k6.io/k6-utils/1.3.0/index.js';

import { sleep, group, check } from 'k6'
import http from 'k6/http'

export const options = {
  stages: [
    { duration:  '5s', target:  20 },
    { duration: '20s', target: 100 },
    { duration:  '5s', target: 300 },
  ],
  userAgent: 'k6-load-test/1.0',

  ext: {
    loadimpact: {
      projectID: 3628456,
      name: "week-entry-page-load"
    }
  }
};

/**
 * k6 load test script against freetime ftclient week-entry page.
 */
export default function main() {
  let response

  const baseUrl = `${__ENV.BASE_URL}`;

  tagWithCurrentStageIndex();
  tagWithCurrentStageProfile();

  group('week-entry-page-client', function () {
    response = http.get(baseUrl, {
      headers: {
        Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
      },
    })
    check(response, {
      'index is status 200': (r) => r.status === 200,
    });

    response = http.get(`${baseUrl}/static/js/main.08fb718a.js`, {
      headers: {
        Connection: 'keep-alive',
        'Accept-Encoding': 'gzip, deflate, br',
      },
    })
    check(response, {
      'js is status 200': (r) => r.status === 200,
      'body has bytes': (r) => r.body.length >= 20, 
    });

    response = http.get(`${baseUrl}/static/css/main.e2e1009c.css`, {
      headers: {
        Accept: 'text/css,*/*;q=0.1',
        'Accept-Encoding': 'gzip, deflate, br',
      },
    })
    check(response, {
      'css is status 200': (r) => r.status === 200,
      'body has bytes': (r) => r.body.length >= 20, 
    });
  });

  group('week-entry-page-rest', function () {
    response = http.get(`${baseUrl}/ftserver/info`, {
      headers: {
        Accept: 'application/json'
      },
    })
    check(response, {
      'is status 200': (r) => r.status === 200,
      'verify info text': (r) => r.body.includes('freetime server'),
    });
  });



  group('week-entry-page-images', function () {
    response = http.get(`${baseUrl}/logo192.png`)
    response = http.get(`${baseUrl}/favicon.ico`)
    check(response, {
      'is status 200': (r) => r.status === 200,
    });
 });

  sleep(1)
}

export function handleSummary(data) {
  return {
    "reports/k6.call-entry-page.report.html": htmlReport(data),
  };
}