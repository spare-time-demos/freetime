package freetime;

import java.util.*;

import io.gatling.javaapi.core.*;
import io.gatling.javaapi.http.*;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;

/** load test week-entry-page. */
public class FreetimeRecordedSimulation extends Simulation {

  private HttpProtocolBuilder httpProtocol = http
    .baseUrl(System.getProperty("baseUrl"))
    .inferHtmlResources()
    .acceptHeader("image/avif,image/webp,*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("de,en-US;q=0.7,en;q=0.3")
    .userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) gatling load test");
  
  private Map<CharSequence, String> headers_0 = Map.ofEntries(
    Map.entry("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache")
  );
  
  private Map<CharSequence, String> headers_1 = Map.ofEntries(
    Map.entry("Accept", "*/*"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache")
  );
  
  private Map<CharSequence, String> headers_2 = Map.ofEntries(
    Map.entry("Accept", "text/css,*/*;q=0.1"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache")
  );
  
  private Map<CharSequence, String> headers_3 = Map.ofEntries(
    Map.entry("Accept", "application/json"),
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache")
  );
  
  private Map<CharSequence, String> headers_4 = Map.ofEntries(
    Map.entry("Cache-Control", "no-cache"),
    Map.entry("Pragma", "no-cache")
  );


  private ScenarioBuilder scn = scenario("Freetime_WeekEntry_RecordedSimulation")
    .exec(
      http("request_index_html")
        .get("/")
        .headers(headers_0).check(status().is(200))
        .resources(
          http("request_main_js")
            .get("/static/js/main.08fb718a.js")
            .headers(headers_1),
          http("request_main_css")
            .get("/static/css/main.e2e1009c.css")
            .headers(headers_2),
          http("request_rest_info")
            .get("/ftserver/info")
            .headers(headers_3)
            .check(status().is(200)),
          http("request_logo_png")
            .get("/logo192.png")
            .headers(headers_4),
          http("request_favicon_ico")
            .get("/favicon.ico")
            .headers(headers_4)
        )
    );

  {
    System.out.println("Testing " + System.getProperty("baseUrl"));

	  setUp(scn.injectOpen(
            atOnceUsers(2),
            rampUsers(10).during(10),
            constantUsersPerSec(20).during(20),
            rampUsersPerSec(30).to(80).during(10),
            stressPeakUsers(500).during(20)
             ))
            .protocols(httpProtocol);
  }
}
