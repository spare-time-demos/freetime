import { test, expect } from '@playwright/test';

test.describe("surf to playwright.dev", () => {

  test.describe("check playwright.dev site", () => {

    test('has title', async ({ page }) => {

      await page.goto('https://playwright.dev/');

      await expect(page).toHaveTitle(/Playwright/);
    });

    test('playwright.dev has get started link', async ({ page }) => {

      test.info().annotations.push({ type: 'document', description: 'screenshot' });

      await test.step('navigate: Get started', async () => {
        await page.goto('https://playwright.dev/');
        await page.getByRole('link', { name: 'Get started' }).click();

      });

      await expect(page).toHaveURL(/.*intro/);

      await test.step('make screenshot', async () => {
        await page.screenshot({ path: 'playwright-test-results/playwright.dev.screenshot.png' });
      });
    })

  })
});
