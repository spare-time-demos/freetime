import { test, expect } from '@playwright/test';

test.describe("freetime basic runthrough", () => {

    let url = '/?lang=en';

    test.describe("use week entry page", () => {

        test('app has freetime in title', async ({ page }) => {

            test.info().annotations.push({ type: 'category', description: 'week entry page' });
            test.info().annotations.push({ type: 'document', description: 'screenshot' });
            test.info().annotations.push({ type: 'test_type', description: 'smoketest' });


            await test.step('navigate to week-entry-page', async () => {
                await page.goto(url);
                await expect(page).toHaveTitle(/freetime/);
            });


            await test.step('make screenshot', async () => {
                await page.screenshot({ path: 'playwright-test-results/week-entry-page.png' });
            });

        });

        test('time entry is possible', async ({ page }) => {

            test.info().annotations.push({ type: 'test_type', description: 'smoketest' });

            await page.goto(url);

            await test.step('add hours', async () => {
                await page.getByRole('row', { name: 'task a task 0' }).locator('input[name="mon"]').fill('3');

                await page.getByRole('row', { name: 'task another task 0' }).locator('input[name="tue"]').fill('2');
            });

            await test.step('check daily sums', async () => {
                expect(page.locator('.day-sum-0')).toHaveText("3")
                expect(page.locator('.day-sum-1')).toHaveText("2")
                expect(page.locator('.day-sum-2')).toHaveText("0")
            });

        })

        test('change language de and en', async ({ page }) => {

            test.info().annotations.push({ type: 'category', description: 'week entry page' });
            test.info().annotations.push({ type: 'document', description: 'screenshot' });
            test.info().annotations.push({ type: 'test_type', description: 'smoketest' });


            await test.step('navigate to week-entry-page', async () => {
                await page.goto(url);
                await expect(page.getByTestId("header")).toHaveText("freetime client!");
                await expect(page.getByTestId("task-header")).toHaveText("Task");
            });

            await test.step('make screenshot en', async () => {
                await page.screenshot({ path: 'playwright-test-results/week-entry-page.en.png' });
            });

            await test.step('navigate to week-entry-page', async () => {
                await page.goto(url);
                await page.getByRole('button', { name: 'de' }).click();
                await expect(page.getByTestId("header")).toHaveText("Freetime Client!");
                await expect(page.getByTestId("task-header")).toHaveText("Aufgabe");
            });

            await test.step('make screenshot de', async () => {
                await page.screenshot({ path: 'playwright-test-results/week-entry-page.de.png' });
            });

        });


    })
});
