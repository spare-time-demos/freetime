# create a container for running e2e tests
# runs a playwright test suite

FROM  mcr.microsoft.com/playwright:v1.31.2-focal
LABEL org.opencontainers.image.authors="man@home"

# Set the working directory to /app inside the container
WORKDIR fttest

# Copy required gradle and liquibase db migrations
COPY tests tests
COPY package*.json .
COPY playwright* .

RUN mkdir playwright-report
RUN mkdir playwright-test-results

RUN echo "this folder will contain tests results" > playwright-test-results/readme.txt

VOLUME /fttest/playwright-report
VOLUME /fttest/playwright-test-results

# execute db migration with gradle liquibase tasks
ENTRYPOINT ["npm"]
CMD ["run", "test:ci:complete"]