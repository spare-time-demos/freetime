# create a container for prometheus

FROM prom/prometheus
LABEL org.opencontainers.image.authors="man@home"

ADD prometheus.yml /etc/prometheus/