import { LinguiConfig } from '@lingui/conf'

const config: Partial<LinguiConfig> = {
    locales: [ "en", "de" ],
    sourceLocale: "en",
    catalogs: [ {
        path: "<rootDir>/src/locales/{locale}/messages",
        include: [ "<rootDir>/src" ],
        exclude: ["**/node_modules/**"]
    } ],
    compileNamespace: "ts",
    format: "po",
};

export default config;
