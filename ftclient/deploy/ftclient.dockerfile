# create a container for ftclient (nginx+react client)

# stage 0: build the client
# -------------------------

FROM node:lts-alpine as builder
# Set the working directory to /app inside the container
WORKDIR app
# Copy app files
COPY . .
# Install dependencies (npm ci makes sure the exact versions in the lockfile gets installed)
RUN npm ci 
# Build the app
RUN npm run i18n:compile
RUN npm run build

# stage 1: bundle client static assets with nginx
# -----------------------------------------------

FROM nginx:stable-alpine
ENV NODE_ENV production
# Copy built assets
# COPY build /usr/share/nginx/html
COPY --from=builder /app/build /usr/share/nginx/html
# Add your nginx.conf
COPY deploy/nginx.conf /etc/nginx/conf.d/default.conf
# Expose port
EXPOSE 80
# Start nginx
CMD ["nginx", "-g", "daemon off;"]
