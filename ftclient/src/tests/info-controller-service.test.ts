import { InfoControllerService } from "../apiclient";

describe( 'test access server api via rest', () => {

  test.skip('fetch info data', async () => {

    const info = await InfoControllerService.info();

    expect(info.name).toContain('freetime server');  

  });

});
