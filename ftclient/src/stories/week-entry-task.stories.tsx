import React from 'react';
import { WeekEntryTask } from "../entry/week-entry-task";
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { Task } from "../pd/task";
import { WeekEntryTaskHolder } from "../pd/week-entry-task-holder";

export default {
    title: "Components/WeekEntryTask",
    component: WeekEntryTask,
} as ComponentMeta<typeof WeekEntryTask>;

const wet = new WeekEntryTaskHolder( new Task(1,"a Task"), new Date());

const wet2 = new WeekEntryTaskHolder( new Task(2,"a longer Task"), new Date());
wet2.timeEntries[0] = 1;
wet2.timeEntries[3] = 3.4;

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof WeekEntryTask> = (args) => <WeekEntryTask {...args} />;

export const Primary = Template.bind({});
export const FractionalHours = Template.bind({});

Primary.args = {
    weekEntryTask: wet, 
    onTaskChange: function(wet: WeekEntryTaskHolder) { console.debug("on change" + wet)}
};

FractionalHours.args = {
    weekEntryTask: wet2, 
    onTaskChange: function(wet: WeekEntryTaskHolder) {console.debug("on change " + wet)}
};
