import { TrackClient } from "../track-client";
import { TrackingControllerService} from "../../apiclient";

jest.mock('../../apiclient/services/TrackingControllerService');

const mockService = jest.mocked(TrackingControllerService);

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    mockService.mockClear();
});

describe( 'send tracking data', () => {

  test('actual fetch attempt -nok', async () => {

    const client = new TrackClient()
    
    const data = { clientId: "xy", event: "nix"}
    const result = await client.track(data);

    expect(result).toBeTruthy();
    expect(mockService.track).toHaveBeenCalled();

  });

  test('send tracking structure', async () => {

    const client = new TrackClient()
    
    const data = { name: "xy" }

    expect(mockService.track).not.toHaveBeenCalled();

    const result = await client.track(data);
    expect(mockService.track).toHaveBeenCalled();
    expect(mockService.track.mock.calls[0][0]).toHaveProperty("client_id");
    expect(mockService.track.mock.calls[0][0]).toHaveProperty("events");
    expect(mockService.track.mock.calls[0][0]).toHaveProperty("user_id");
    expect(mockService.track.mock.calls[0][0]).toHaveProperty("user_id");

    expect(result).toBeTruthy();
  });


});
