import { TrackingControllerService, TrackingData, TrackingDataEvent, TrackingDataEventParam } from "../apiclient";


/** send client side tracking data to ftserver. */
export class TrackClient {

    // eslint-disable-next-line
    async track(data: Partial<any>) : Promise<boolean> {

        console.log("track: " + data);

        let tid = sessionStorage["ftclient.tid"];
        if(! tid) {
            tid = Math.floor(Math.random() * 100); // TBD: that is NOT a good unique id.
            sessionStorage["ftclient.tid"] = tid;
        }

        const params : TrackingDataEventParam = {
            page_title: data.page,            
            session_id: tid,
            engagement_time_msec: 1     // TBD: calculate time diffs
        };

        const event : TrackingDataEvent = {
            name: data.name,
            params: params
        };

        const td: TrackingData = {
            client_id: tid,
            user_id: "ftclient_user",
            events: [event]
        } 
        
        try {
            await TrackingControllerService.track(td);
            return true;
        } catch( error ) {
            console.warn("error sending tracking data " + error)
            return false;
        }
    }

}