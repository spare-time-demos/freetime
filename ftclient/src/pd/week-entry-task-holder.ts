import { Task } from "./task";
import { format } from 'date-fns'


/** one line of task working hours. */
export class WeekEntryTaskHolder {

    /** task to be worked on. */
    task: Task;

    /** worked hours per weekday. */
    timeEntries: number[];

    /* first day of week. */
    date: Date;

    constructor(task: Task, date: Date) {
        this.task        = task
        this.timeEntries = [0,0,0,0,0,0,0];
        this.date        = date;
    }

    key() : string {
        const key = `t${this.task.id}_d${format(this.date,'yyyy_MM_dd')}` 
        return key;
    }

    taskSum(): number {
        let sum = 0;
        if(this.timeEntries == null) {
            sum = 0;
        } else {
            sum = this.timeEntries.reduce( (sum, current) => sum + current, 0);
        }
        return sum;
    }

    changeHour(indexDay: number, newHoursValue: string) : WeekEntryTaskHolder {

        // newWet.timeEntries = this.timeEntries.slice();
        this.timeEntries[indexDay] = + newHoursValue;

        return this;       
    } 
}