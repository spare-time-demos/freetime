import { Task } from "./task";
import { WeekEntryTaskHolder } from "./week-entry-task-holder";

import { previousMonday, isMonday } from 'date-fns'

export class WeekEntryHolder {

    weekEntryTasks: WeekEntryTaskHolder[];
    readonly date: Date;

    constructor(date: Date) {
        this.date = isMonday(date) ? date : previousMonday(date);
        this.weekEntryTasks = [
            new WeekEntryTaskHolder(new Task(1, "a task"), this.date), 
            new WeekEntryTaskHolder(new Task(2, "another task"), this.date)
        ];
    }

    updateTask(changedWet: WeekEntryTaskHolder) {
        const i = this.weekEntryTasks.findIndex((wet) => wet.task.id === changedWet.task.id)
        this.weekEntryTasks[i] = changedWet;
    }

    sumOfDay(index: number) : number {

        const sum = this.weekEntryTasks.reduce( (sum, current) => sum + current.timeEntries[index], 0)
        return sum;
   }
}