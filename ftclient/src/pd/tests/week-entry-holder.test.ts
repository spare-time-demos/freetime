import { WeekEntryHolder } from "../week-entry-holder";

describe( 'week entry holder ', () => {

  test('init', () => {
    const d = new Date(2023,3,12);
    const dWeekStart = new Date(2023,3,10);
    const we = new WeekEntryHolder(d);

    expect(we.date).toEqual(dWeekStart);

  });

  test('sum of day', () => {
    const d = new Date(2023,3,12);
    const we = new WeekEntryHolder(d);

    expect(we.sumOfDay(0)).toEqual(0);

  });

});