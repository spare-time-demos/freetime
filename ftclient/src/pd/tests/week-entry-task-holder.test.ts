import { Task } from "../task";
import { WeekEntryTaskHolder } from "../week-entry-task-holder";

const task = new Task(1,"task1");

describe( 'week entry task holder ', () => {

  test('with expected key calculated', () => {
    const d = new Date(2001,3,12);
    const wet = new WeekEntryTaskHolder(task, d);

    expect(wet.key()).toBe('t1_d2001_04_12');
  });

  test('with correct sum', () => {
    const d = new Date(2001,3,12);
    const wet = new WeekEntryTaskHolder(task, d);

    wet.changeHour(0, '2');
    wet.changeHour(1, '3');

    expect(wet.taskSum()).toBe(5);
  });

  test('with seven day slots', () => {
    const d = new Date(2001,3,12);
    const wet = new WeekEntryTaskHolder( new Task(1,"task1"), d);

    expect(wet.timeEntries.length).toBe(7);
  });
});
