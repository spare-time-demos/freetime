import { Trans } from '@lingui/react';
import { i18n } from '@lingui/core';

import { addWeeks } from 'date-fns'
import { Component } from 'react'; // let's also import Component
import track from 'react-tracking';
import { InfoControllerService, OpenAPI } from '../apiclient';
import { WeekEntryHolder } from '../pd/week-entry-holder';
import { WeekEntryTaskHolder } from '../pd/week-entry-task-holder';
import { WeekEntryTask } from './week-entry-task';

import './week-entry.css';

import { AppBar, Button, Chip, Container, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TableRow, Toolbar } from '@mui/material';
import ArrowBackIosSharpIcon from '@mui/icons-material/ArrowBackIosSharp';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';

type WeekEntryProps = Record<string, never>

type WeekEntryState = {
    weekEntry: WeekEntryHolder;
    serverInfo: string;
}

/** ui page: entry one week of work data. */
@track({ name: "page_view", page: 'WeekEntry' })
export class WeekEntry extends Component<WeekEntryProps, WeekEntryState> {

    constructor(props: WeekEntryProps) {
        super(props);
        this.state = { weekEntry: new WeekEntryHolder(new Date()), serverInfo: "no server" };
    }

    componentDidMount(): void {

        // network call demo..
        const infoProm = InfoControllerService.info()
        infoProm.then(info => this.setState({ serverInfo: info.name + " " + info.currentTime }));
    }

    @track((_props, _state, [deltaWeeks]: [number]) => ({ name: "click", action: 'changeWeek', details: `current ${_state.weekEntry.date} add ${deltaWeeks}` }))
    changeWeek(deltaWeeks: number) {
        const nextWeek = addWeeks(this.state.weekEntry.date, deltaWeeks);
        this.setState({ weekEntry: new WeekEntryHolder(nextWeek) });
    }

    @track((_props, _state, [language]: [string]) => ({ name: "click", action: 'changeLanguage', details: `${language}` }))
    changeLanguage(language: string) {
        i18n.activate(language);
    }

    /** hour input on task was changed. */
    changeTask(changedWeekEntryTask: WeekEntryTaskHolder) {

        const weh = this.state.weekEntry;
        weh.updateTask(changedWeekEntryTask);

        this.setState({ weekEntry: weh });
    }

    /** show one row of entry data */
    renderTaskRows() {
        return this.state.weekEntry.weekEntryTasks.map(
            weth => <WeekEntryTask key={weth.key()} weekEntryTask={weth} onTaskChange={(wet) => this.changeTask(wet)} />
        );
    }

    /** show sum of day footer. */
    renderColumnSums() {
        return [0, 1, 2, 3, 4, 5, 6].map(
            index =>
                <td className={'day-sum-' + index + ' ' + (index === 0 ? 'stickyColumn' : '')}
                    key={index}>
                    <Chip label={this.state.weekEntry.sumOfDay(index)} />
                </td>
        );
    }

    render() {

        return <>
            <h2><Trans id="week-entry.header">my week entry</Trans>&nbsp;
                <Button onClick={() => this.changeWeek(-1)}><ArrowBackIosSharpIcon /></Button>
                &nbsp; {i18n.date(this.state.weekEntry.date)} &nbsp;
                <Button  data-testid="btn-next-week" onClick={() => this.changeWeek(1)}><ArrowForwardIosSharpIcon /></Button>
            </h2>

            <Container className='content' sx={{ width: '100%', mb: 2 }}>
                <form>
                        <TableContainer>
                            <Table aria-label="week data table" stickyHeader>
                                <TableHead>
                                    <TableRow>
                                        <TableCell className="stickyColumn" data-testid="task-header"><Trans id="week-entry.task">Task</Trans></TableCell>
                                        <TableCell><Trans id="Monday.short">Mon</Trans></TableCell>
                                        <TableCell><Trans id="Tuesday.short">Tue</Trans></TableCell>
                                        <TableCell><Trans id="Wednesday.short">Wed</Trans></TableCell>
                                        <TableCell><Trans id="Thursday.short">Thu</Trans></TableCell>
                                        <TableCell><Trans id="Friday.short">Fri</Trans></TableCell>
                                        <TableCell><Trans id="Saturday.short">Sat</Trans></TableCell>
                                        <TableCell><Trans id="Sunday.short">Sun</Trans></TableCell>
                                        <TableCell><Trans id="week-entry.sum">sum</Trans></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.renderTaskRows()}
                                </TableBody>
                                <TableFooter className="weekFooter">
                                    <tr className='rowWeekDailySum'><td></td>
                                        {this.renderColumnSums()}
                                        <td></td></tr>
                                </TableFooter>
                            </Table>
                        </TableContainer>
                </form>
                <br />
                <br />
            </Container>

            <AppBar color="primary" sx={{top: 'auto', bottom: 0 }}>
            <Toolbar>
                <span>env: 
                    <Chip label={process.env.REACT_APP_FREETIME_ENVIRONMENT} variant="filled" />&nbsp;
                    server:&nbsp;
                    <Chip label={this.state.serverInfo} variant="filled" />
                    at
                    <Chip label={OpenAPI.BASE} variant="filled" />
                    &nbsp;
                    client:&nbsp;
                    {process.env.PUBLIC_URL}
                </span>
                &nbsp;
                <Button variant="contained" onClick={() => this.changeLanguage("en")}>en</Button>
                &nbsp;
                <Button variant="contained" onClick={() => this.changeLanguage("de")}>de</Button>
                </Toolbar>
            </AppBar>
        </>
    }
}