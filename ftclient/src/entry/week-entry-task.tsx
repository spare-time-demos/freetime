import React, { Component } from 'react';
import { WeekEntryTaskHolder } from '../pd/week-entry-task-holder';

import TextField from '@mui/material/TextField';
import { Chip, TableCell, TableRow } from '@mui/material';

import './week-entry-task.css';

type WeekEntryTaskState = {
    weekEntryTask: WeekEntryTaskHolder;
}

interface WeekEntryTaskProps {
    weekEntryTask: WeekEntryTaskHolder;
    onTaskChange: (changedWeekEntryTask: WeekEntryTaskHolder) => void;
}


/** ui component: entry one task data in week entry. */
export class WeekEntryTask extends Component<WeekEntryTaskProps, WeekEntryTaskState> {


    handleHourChange(indexDay: number, event: React.ChangeEvent<HTMLInputElement>) {
        const newWet = this.props.weekEntryTask.changeHour(indexDay, event.target.value);
        this.props.onTaskChange(newWet);
    }

    renderHourColumn(name: string, index: number) : JSX.Element {
        return <TextField 
                    name={name} 
                    type="number" 
                    size="small" 
                    placeholder="0"
                    value={this.state?.weekEntryTask?.timeEntries[index]}
                    sx={{minWidth:70}}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.handleHourChange(index, event) }}>
               </TextField>
        ;
    }

    render() {

        return <TableRow className={'t-' + this.props.weekEntryTask.task.id}>
            <TableCell className='stickyColumn'>
                task {this.props.weekEntryTask.task.name} 
            </TableCell>
            
            <TableCell>
                {this.renderHourColumn('mon', 0)}
            </TableCell>
            <TableCell>                
                {this.renderHourColumn('tue', 1)}
            </TableCell>
            <TableCell>
                {this.renderHourColumn('wed', 2)}
            </TableCell>
            <TableCell>                
                {this.renderHourColumn('thu', 3)}
            </TableCell>
            <TableCell>
                {this.renderHourColumn('fri', 4)}
            </TableCell>
            <TableCell>                
                {this.renderHourColumn('sat', 5)}
            </TableCell>
            <TableCell>
                {this.renderHourColumn('sun', 6)}
            </TableCell>

            <TableCell className={'sum-t-' + this.props.weekEntryTask.task.id}>
                <Chip label={this.props.weekEntryTask.taskSum()} />
            </TableCell>
        </TableRow>
    }
}