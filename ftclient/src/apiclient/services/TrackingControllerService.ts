/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { TrackingData } from '../models/TrackingData';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class TrackingControllerService {

    /**
     * tracking endpoint
     * receive client side tracking information
     * @param requestBody 
     * @returns any ok
     * @throws ApiError
     */
    public static track(
requestBody: TrackingData,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/ftserver/track',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
