/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Info } from '../models/Info';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class InfoControllerService {

    /**
     * application info
     * get basic application information
     * @returns Info ok
     * @throws ApiError
     */
    public static info(): CancelablePromise<Info> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/ftserver/info',
        });
    }

}
