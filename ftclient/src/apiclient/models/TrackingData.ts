/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { TrackingDataEvent } from './TrackingDataEvent';

export type TrackingData = {
    client_id?: string;
    user_id?: string;
    events?: Array<TrackingDataEvent>;
};
