/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { TrackingDataEventParam } from './TrackingDataEventParam';

export type TrackingDataEvent = {
    name?: string;
    params?: TrackingDataEventParam;
};
