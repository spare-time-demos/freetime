/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * application info
 */
export type Info = {
    /**
     * application name
     */
    name: string;
    /**
     * current server time
     */
    currentTime: string;
};
