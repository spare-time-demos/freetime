/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TrackingDataEventParam = {
    page_location?: string;
    page_title?: string;
    engagement_time_msec?: number;
    session_id?: string;
};
