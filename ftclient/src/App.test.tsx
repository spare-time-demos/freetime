import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { I18nProvider } from '@lingui/react';

import { i18n } from '@lingui/core'
import { messages as enMessages } from './locales/en/messages'
import { en, de } from 'make-plural/plurals';


i18n.load({
  en: enMessages
})

i18n.loadLocaleData( {
  en: { plurals: en },
  de: { plurals: de }
});

i18n.activate('en')

const TestingProvider = ({ children }: {children : React.ReactNode}) => (
  <I18nProvider i18n={i18n}>
    {children}
  </I18nProvider>
)

test('rendering freetime app finds title', () => {

  render(<App />, { wrapper: TestingProvider });
  const linkElement = screen.getByText(/freetime client/i);
  expect(linkElement).toBeInTheDocument();
});
