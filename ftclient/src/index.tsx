import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { i18n } from '@lingui/core'
import { I18nProvider } from '@lingui/react'
import { detect, fromUrl, fromStorage, fromNavigator } from "@lingui/detect-locale"

import { messages as enMessages } from './locales/en/messages'
import { messages as deMessages } from './locales/de/messages'
import { en, de } from 'make-plural/plurals';
import { StyledEngineProvider } from '@mui/material';

// -- multi language init

i18n.load({
  en: enMessages,
  de: deMessages,
})

i18n.loadLocaleData({
  en: { plurals: en },
  de: { plurals: de }
});

const detectedLanguage = detect(
  fromUrl("lang"),
  fromStorage("lang"),
  fromNavigator(),
  () => "en"
) ?? "en";

i18n.activate(detectedLanguage);

// -- run the app
// -- with i18n library and material ui.
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <StyledEngineProvider injectFirst>
      <I18nProvider i18n={i18n}>
        <App />
      </I18nProvider>
    </StyledEngineProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
