import { Trans } from "@lingui/macro";
import { AppBar, Toolbar, Typography } from "@mui/material";
import React from 'react';
import track from 'react-tracking';
import { OpenAPI } from './apiclient';
import './App.css';
import './entry/week-entry';
import { WeekEntry } from './entry/week-entry';
import { TrackClient } from './track/track-client';

/** init api server url from .env values */
OpenAPI.BASE = process.env.REACT_APP_FREETIME_FTSERVER_URL == null
  ? OpenAPI.BASE
  : process.env.REACT_APP_FREETIME_FTSERVER_URL;

/** react.js app freetime client. */
@track({}, { process: (ownTrackingData) => ownTrackingData.page ? { action: 'pageview' } : null })
class App extends React.Component {

  render() : JSX.Element {

    //
    return (
      <div className="app">

        <AppBar position="static" className="app-header">
          <Toolbar>
            <Typography variant="h2" data-testid="header">
                <Trans id="app.header">freetime client!</Trans>
            </Typography>
          </Toolbar>
        </AppBar>

        <div>
          <WeekEntry />
        </div>
      </div>
    );
  }
}

/** tracking decorated. */
const TrackedApp = track(
  // app-level tracking data
  { app: "Freetime" },
  {
    dispatch: (data) => {
      (new TrackClient()).track(data);
    }
  }
)(App);

export default TrackedApp;
