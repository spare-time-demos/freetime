= freetime api backend server

* run server `gradlew bootRun`
** http://localhost:8080
** http://localhost:8080/v3/api-docs
** http://localhost:8080/swagger-ui/index.html
** http://localhost:8080/ftserver/info

* run tests `gradlew test`

* generate openapi schema: `./gradlew generateOpenApiDocs`
* vulnerability scan: `./gradlew dependencyCheckAnalyze`

== more infos

* project doc page: https://spare-time-demos.gitlab.io/freetime

== Reference Documentation

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.0.2/gradle-plugin/reference/html/)
* [Distributed Tracing Reference Guide](https://micrometer.io/docs/tracing)
* [Getting Started with Distributed Tracing](https://docs.spring.io/spring-boot/docs/3.0.2/reference/html/actuator.html#actuator.micrometer-tracing.getting-started)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#web)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#actuator)
* [Prometheus](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#actuator.metrics.export.prometheus)
* [Spring REST Docs](https://docs.spring.io/spring-restdocs/docs/current/reference/html5/)

