package org.manathome.freetime.ftserver.tracking;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrackingDataTest {

  private String toJson(TrackingData td) throws Exception { return new ObjectMapper().writeValueAsString(td); }

  @Test
  void validateBaseJsonSerialization() throws Exception {

      String expected =
              """
              {"client_id":"c1","user_id":"u1"}""";

      var td = new TrackingData("c1", "u1", null);

      assertThat(toJson(td)).isEqualTo(expected);
  }

  @Test
  void validatePageViewJsonSerialization() throws Exception {

    String expected =
            """
            {"client_id":"c1","user_id":"u1","events":[{"name":"aname"}]}""";

    TrackingDataEvent[] events  = { new TrackingDataEvent("aname", null ) };

    var td = new TrackingData(
            "c1",
            "u1",
             events
            );

    assertThat(toJson(td)).isEqualTo(expected);
  }

  @Test
  void validatePageViewDetailsJsonSerialization() throws Exception {

    String expected =
            """
            {"client_id":"c1","user_id":"u1","events":[{"name":"aname","params":{"page_location":"aLoc","page_title":"aTitle","engagement_time_msec":3,"session_id":"aSess"}}]}""";

    TrackingDataEvent[] events  = { new TrackingDataEvent("aname",
            new TrackingDataEventParam( "aLoc", "aTitle", Long.valueOf(3), "aSess" ) ) };

    var td = new TrackingData(
            "c1",
            "u1",
            events
    );

    assertThat(toJson(td)).isEqualTo(expected);
  }

}
