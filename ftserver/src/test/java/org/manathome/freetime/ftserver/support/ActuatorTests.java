package org.manathome.freetime.ftserver.support;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

/** check activated /actuator endpoint. */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ActuatorTests {
  @Value(value="${local.server.port}")
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  void testActuatorHealthMessage() throws Exception {

    assertThat(
            this.restTemplate.getForObject("http://localhost:" + port + "/actuator/health",
            String.class))
            .contains("{\"status\":\"UP\"}");
  }
}
