package org.manathome.freetime.ftserver.repository;

import org.junit.jupiter.api.Test;
import org.manathome.freetime.ftserver.pd.FtProject;
import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;

/** test project entity. */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
class ProjectRepositoryTest {

  @Autowired
  ProjectRepository repository;

  @Autowired
  TeamMemberRepository tmRepository;

  @Test
  public void testProjectRead() {
    assertThat(repository).isNotNull();

    var projects = repository.findAll();
    assertThat(projects).isNotNull();
  }

  @Test
  public void testProjectSave() {

    var newProject = new FtProject("unit-test-project1");
    var newLead = new FtTeamMember("unit-test-lead1", "test lead 1");
    newProject.addProjectLead(newLead);

    tmRepository.save(newLead);
    var savedProject = repository.save(newProject);

    assertThat(savedProject.getId()).isNotNull();
    assertThat(savedProject.getProjectLeads().stream().allMatch(tl -> tl.getId() != null)).isTrue();
    assertThat(savedProject.isActive()).isTrue();

    savedProject.setActive(false);
    savedProject.setName(("unit-test-project1-changed"));

    savedProject = repository.save(savedProject);
    assertThat(savedProject.isActive()).isFalse();
    assertThat(savedProject.getName()).contains("changed");
    assertThat(savedProject.hasLead(newLead)).isTrue();

    var projects = repository.findAll();
    final var id = savedProject.getId();
    assertThat(projects).isNotNull();
    assertThat(StreamSupport
            .stream(projects.spliterator(), false)
            .filter(p -> p.getId().equals(id))
            .findFirst().orElseThrow().getName()).isEqualTo(savedProject.getName());
  }

  @Test
  public void testProjectByLead() {

    var newProject = new FtProject("unit-test-project1");
    var newLead = new FtTeamMember("unit-test-lead1", "test lead 1");
    newProject.addProjectLead(newLead);

    var leadId = tmRepository.save(newLead).getId();
    var savedProject = repository.save(newProject);

    var projects = repository
            .findByProjectLeads(savedProject.getProjectLeads().stream().findFirst().orElseThrow());
    assertThat(projects).isNotNull();

    assertThat(StreamSupport.stream(projects.spliterator(), false)
            .allMatch(p -> p.getProjectLeads().stream().anyMatch(l -> l.getId().equals(leadId))))
            .isTrue();
  }

}

