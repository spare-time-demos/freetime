package org.manathome.freetime.ftserver.repository;

import org.junit.jupiter.api.Test;
import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class TeamMemberRepositoryTest {

  @Autowired
  TeamMemberRepository repository;

  @Test
  public void storeMember() {
    assertThat(repository).isNotNull();

    var t = new FtTeamMember("t1", "test-member 1");
    var savedT = repository.save(t);
    assertThat(savedT).isNotNull();
    assertThat(savedT.getId()).isGreaterThan(0);
  }

}
