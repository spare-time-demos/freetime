package org.manathome.freetime.ftserver.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TrackingControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  void shouldSendTrackingData() throws Exception {

    var postData =
            """
            {"client_id":"UNIT_TEST_CONTROLLER","user_id":"UNIT_TEST_USER","events":[{"page_view":"UNIT_TEST_TRACKER","params":{"page_location":"UNIT_TEST_TRACKER","page_title":"Unit Test Tracker","engagement_time_msec":3,"session_id":"4711"}}]};
            """;

    this.mockMvc
            .perform(post("/ftserver/track")
                            .content(postData)
                            .contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk());
  }

}
