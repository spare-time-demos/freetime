package org.manathome.freetime.ftserver.api;

import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.freetime.ftserver.repository.TeamMemberRepository;
import org.manathome.freetime.ftserver.repository.WorkDoneRepository;
import org.manathome.freetime.ftserver.service.WorkWeekService;
import org.manathome.freetime.ftserver.support.JsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "build/snippets")
@ExtendWith(RestDocumentationExtension.class)
public class WorkWeekControllerTest {

  private static final Logger logger = LoggerFactory.getLogger(WorkWeekControllerTest.class);

  @Autowired
  private MockMvc mvc;

  @Autowired
  private WorkWeekService workWeekService;

  @Autowired
  private WorkDoneRepository workDoneRepository;

  @Autowired
  private TeamMemberRepository teamMemberRepository;

  @Autowired
  private EntityManager em;

  @Test
  public void testGetWorkWeek() throws Exception {

    var startOfWeekDate = LocalDate.now().plusDays(-5).toString(); // calc todo start of week

    mvc.perform(get("/ftserver/workweek/" + startOfWeekDate))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.from", org.hamcrest.Matchers.is(startOfWeekDate)))
            .andExpect(jsonPath("$.until", org.hamcrest.Matchers.containsString("-")))
            .andDo(document("/ftserver/workweek/"
                    + WorkWeekController.FROM_ISO_DATE_STRING + "/GET"));
    ;
  }

  @Test
  @Transactional
  public void testChangeWorkWeek() throws Exception {

    var startOfWeekDate = LocalDate.now().plusDays(-5); // calc todo start of week

    //fake, constant user..
    final var teamMember = teamMemberRepository.findByUserId("tlead1").orElseThrow();

    final var ww = workWeekService.getWorkWeek(teamMember, startOfWeekDate);
    assertThat(ww.getTaskWeeks().length).isGreaterThan(1);

    final var newWorkHoursAssigned00       = ww.getTaskWeeks()[0]
            .getWorkHours()[0]
            +2; // first task, first day

    ww.getTaskWeeks()[0].getWorkHours()[0] = newWorkHoursAssigned00;

    final var newWorkHoursAssigned03       = ww.getTaskWeeks()[0]
            .getWorkHours()[3]
            +1; // first task, fourth day

    ww.getTaskWeeks()[0].getWorkHours()[3] = newWorkHoursAssigned03;

    var firstDayWorkHours = ww.getTaskWeeks()[0].getWorkHours()[0];
    assertThat(firstDayWorkHours).isGreaterThan(1);

    var workWeekJson = JsonHelper.asJsonString(ww);

    logger.debug("post data: " + workWeekJson);

    mvc.perform(put("/ftserver/workweek/" + startOfWeekDate)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(workWeekJson))
            .andDo(print())
            .andExpect(status().isOk())
            .andDo(document("/ftserver/workweek/"
                    + WorkWeekController.FROM_ISO_DATE_STRING + "/PUT"));
    ;

    em.flush();

    // read changed values back from database:

    final var workDone = workDoneRepository
            .findWorkDoneByMember(
                    teamMember,
                    startOfWeekDate,
                    startOfWeekDate.plusDays(7))
            .collect(Collectors.toList());


    assertThat(workDone
            .stream()
            .filter(wd -> wd.getTaskId().equals(ww.getTaskWeeks()[0].getTaskId())
                    && wd.getDateAt().isEqual(startOfWeekDate))
            .findFirst()
            .get()
            .getWorkHours())
            .as("work 0 day hour of " + ww.getTaskWeeks()[0].getTaskName())
            .isEqualTo(newWorkHoursAssigned00);

    assertThat(workDone
            .stream()
            .filter(wd -> wd.getTaskId().equals(ww.getTaskWeeks()[0].getTaskId())
                    && wd.getDateAt().isEqual(startOfWeekDate.plusDays(3)))
            .findFirst()
            .get()
            .getWorkHours())
            .as("work 3 day hour of " + ww.getTaskWeeks()[0].getTaskName())
            .isEqualTo(newWorkHoursAssigned03);
  }

}
