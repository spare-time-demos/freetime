package org.manathome.freetime.ftserver.service;

import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.manathome.freetime.ftserver.repository.TeamMemberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class WorkWeekServiceTest {

  private static final Logger logger = LoggerFactory.getLogger(WorkWeekServiceTest.class);

  @Autowired
  WorkWeekService workWeekService;

  @Autowired
  TeamMemberRepository teamMemberRepository;

  @Autowired
  EntityManager em;

  @Test
  public void testUpdateMoreHoursToWorkWeek() {

    final var startOfWeekDate = LocalDate.now().plusDays(10);

    final var teamMember = teamMemberRepository.findByUserId("tlead1").orElseThrow();

    // db read
    final var workWeek = workWeekService.getWorkWeek(teamMember, startOfWeekDate);
    assertThat(workWeek).isNotNull();

    final var oldSum = workWeek.sumWorkHours();

    var firstTask = workWeek.getTaskWeeks()[0];

    logger.debug("before: " + firstTask.toDump());

    firstTask.getWorkHours()[0] = firstTask.getWorkHours()[0] + 2;
    firstTask.getWorkHours()[3] = firstTask.getWorkHours()[3] + 1;
    firstTask.getWorkHours()[5] = firstTask.getWorkHours()[5] + 3;

    logger.debug("change via service to: " + firstTask.toDump());

    // db update
    workWeekService.updateWorkWeek(teamMember, workWeek);

    // db reread
    final var changedWorkWeek = workWeekService.getWorkWeek(teamMember, startOfWeekDate);
    assertThat(changedWorkWeek).isNotNull();

    logger.debug("change done by service: " + changedWorkWeek.getTaskWeeks()[0].toDump());

    assertThat(changedWorkWeek.sumWorkHours())
            .as("6 hours added workweek with update")
            .isEqualTo(oldSum + 2 + 1 + 3);
  }

}
