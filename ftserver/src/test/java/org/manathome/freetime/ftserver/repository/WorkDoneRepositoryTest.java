package org.manathome.freetime.ftserver.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class WorkDoneRepositoryTest {

  @Autowired
  WorkDoneRepository repository;

  @Test
  @Transactional
  void shouldFindDoneWorkYesterdayInDb() {

    var foundWork = repository.findByDateAt(LocalDate.now().minusDays(1)).toList();

    assertThat(foundWork).describedAs("found work").isNotEmpty();

    final var work = foundWork.get(0);
    assertThat(work.getDateAt()).isBefore(LocalDate.now());
    assertThat(work.isValid()).isTrue();
    assertThat(work.getTask()).isNotNull();
    assertThat(work.getTeamMember()).isNotNull();
  }

  @Test
  @Transactional
  void shouldNotFindDoneWorkOfFutureInDb() {

    var foundWork = repository.findByDateAt(LocalDate.now().plusDays(1));

    assertThat(foundWork).describedAs("should not find future work").isEmpty();
  }

}
