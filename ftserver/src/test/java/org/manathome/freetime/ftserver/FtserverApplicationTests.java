package org.manathome.freetime.ftserver;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FtserverApplicationTests {

	@Test
	void contextLoads() {
		assertThat(true).as("spring context loaded, no exception").isTrue();
	}

}
