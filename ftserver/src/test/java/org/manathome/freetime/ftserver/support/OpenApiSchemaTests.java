package org.manathome.freetime.ftserver.support;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

/** openapi schema on /v3/api-docs. */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OpenApiSchemaTests {

  @Value(value="${local.server.port}")
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  void testOpenApiSchemaReturned() throws Exception {

    assertThat(
            this.restTemplate.getForObject(
                    "http://localhost:" + port + "/v3/api-docs",
                    String.class))
            .contains("freetime server OpenAPI Schema");
  }
}
