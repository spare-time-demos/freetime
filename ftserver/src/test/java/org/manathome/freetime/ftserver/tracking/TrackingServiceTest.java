package org.manathome.freetime.ftserver.tracking;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TrackingServiceTest {

  private static final String VALIDATOR_OK_JSON =
          """
          {
            "validationMessages": [ ]
          }
          """
          ;
  @Autowired
  private TrackingService service;

  @Test
  void validatePageViewTrackingCall() throws Exception {

    TrackingDataEvent[] events  = { new TrackingDataEvent("page_view",
            new TrackingDataEventParam( "aLoc", "aTitle", Long.valueOf(3), "aSess" ) ) };

    var td = new TrackingData(
            "c1",
            "u1",
            events
    );

    var debugResult = service.trackDebug(td.toJson());

    assertThat(debugResult)
            .describedAs("ga4 validator not passed")
            .isEqualTo(VALIDATOR_OK_JSON);
  }

  @Test
  void testGa4TrackPageView() throws Exception {

    TrackingDataEvent[] events  = { new TrackingDataEvent("page_view",
            new TrackingDataEventParam(
                    "UNIT_TEST_PAGE",
                    "UNIT_TEST_TITLE",
                    Long.valueOf(1),
                    "UNIT_TEST_SESSION" ) ) };

    var td = new TrackingData(
            "UNIT_TEST_CLIENT",
            "UNIT_TEST",
            events
    );

    var rc = service.track(td.toJson());

    assertThat(rc)
            .describedAs("ga4  passed")
            .isTrue();
  }

}
