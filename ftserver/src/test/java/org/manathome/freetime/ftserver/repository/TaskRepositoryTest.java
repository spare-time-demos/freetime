package org.manathome.freetime.ftserver.repository;

import org.junit.jupiter.api.Test;
import org.manathome.freetime.ftserver.pd.TaskState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

/** test task entity. */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TaskRepositoryTest {

  @Autowired
  TaskRepository repository;

  @Test
  public void testTaskRead() {
    assertThat(repository).isNotNull();

    var tasks = repository.findAll();
    assertThat(tasks).isNotNull();
  }

  @Test
  public void testTaskStore() {

    final var taskId = Long.valueOf(-1);
    var task = repository.findById(taskId);

    assertThat(task.isPresent()).isTrue();
    assertThat(task.orElseThrow().getName()).contains("task");

    task.orElseThrow().setState(TaskState.ACTIVE);
    task.orElseThrow().setName("changed-task-name");

    repository.save(task.orElseThrow());
    task = repository.findById(taskId);
    assertThat(task.orElseThrow().getName()).contains("changed-task");
  }

}
