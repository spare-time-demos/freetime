package org.manathome.freetime.ftserver.pd;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.manathome.freetime.ftserver.support.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

/** week, holds all work and working assignments..
 * @category dto.
 */
@Schema(description = "week, holds all work and working assignments.")
public class FtWorkWeek {

  private static final Logger logger = LoggerFactory.getLogger(FtWorkWeek.class);

  private LocalDate from;

  private LocalDate until;

  private FtTaskWeek[] taskWeeks;

  public FtWorkWeek() {
    logger.trace("WorkWeek.ctor(noarg)");
  }

  /** ctor. */
  public FtWorkWeek(final LocalDate from,
                  final LocalDate until,
                  final Collection<FtTaskAssignment> taskAssignments,
                  final Collection<FtWorkDone> workDone) {

    setFrom(Require.notNull(from, "from"));
    this.until = Require.notNull(until, "until");

    buildTaskWeeks(
            Require.notNull(taskAssignments, "assignments"),
            Require.notNull(workDone, "workDone"));
  }

  private void buildTaskWeeks(
          Collection<FtTaskAssignment> taskAssignments,
          Collection<FtWorkDone> workDone) {

    final var allTaskIds = new HashSet<Long>(); // all distinct tasks from assignments and workDone.
    taskAssignments.forEach(ta -> allTaskIds.add(ta.getTask().getId()));
    workDone.forEach(wd -> allTaskIds.add(wd.getTask().getId()));

    logger.trace("WorkWeek.buildTaskWeeks: " + allTaskIds.size() + " tasks from " + from);

    this.taskWeeks = allTaskIds.stream()
            .map(taskId -> new FtTaskWeek(this.from,
                    taskAssignments
                            .stream()
                            .filter(ta -> ta.getTask().getId().longValue() == taskId.longValue()).findFirst()
                            .orElse(null),
                    workDone.stream().filter(wd -> wd.getTask().getId().longValue() == taskId.longValue())
                            .collect(Collectors.toList())))
            .toArray(FtTaskWeek[]::new);
  }

  @Schema(description = "start of week", example = "2019-01-02")
  public LocalDate getFrom() {
    return from;
  }

  private void setFrom(final LocalDate from) {
    this.from = from;
  }

  @Schema(description = "last day of week", example = "2019-01-08")
  public LocalDate getUntil() {
    return until;
  }

  @Schema(description = "list of tasks for this week")
  public FtTaskWeek[] getTaskWeeks() {
    return taskWeeks;
  }

  /** sum all working hours in all tasks. */
  @JsonIgnore
  public int sumWorkHours() {
    return (int) Arrays.stream(taskWeeks)
            .mapToLong(tw -> tw.sumWorkHours())
            .sum();
  }

  @Override
  public String toString() {
    return "WW[from=" + from + ", sumWorkHours()=" + sumWorkHours() + "]";
  }


  /** checks, throw exceptions. */
  public void validate() {
    Require.notNull(this.getFrom(), "from");
    Require.notNull(this.getTaskWeeks(), "taskWeek");
    if (this.getUntil() != null) {
      Require.isTrue(this.getFrom().isBefore(this.getUntil()),
              "invalid date range " + from + " - " + until);
    }
    Arrays.stream(this.getTaskWeeks()).forEach(tw -> tw.validate());
  }

}

