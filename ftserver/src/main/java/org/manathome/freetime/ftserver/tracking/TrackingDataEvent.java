package org.manathome.freetime.ftserver.tracking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


public record TrackingDataEvent(
        @JsonProperty("name")
        String name,

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("params")
        TrackingDataEventParam params
) { }
