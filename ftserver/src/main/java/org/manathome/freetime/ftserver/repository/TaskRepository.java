package org.manathome.freetime.ftserver.repository;

import org.manathome.freetime.ftserver.pd.FtTask;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

/**
 * database repository for tasks.
 *
 * @author man@home
 */
public interface TaskRepository extends CrudRepository<FtTask, Long> {

  Stream<FtTask> findByProjectIdOrderById(Long projectId);
}
