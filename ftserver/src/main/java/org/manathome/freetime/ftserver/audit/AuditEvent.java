package org.manathome.freetime.ftserver.audit;

public enum AuditEvent {

  APPLICATION_START(1),
  APPLICATION_STOP(2),
  APPLICATION_CONFIGURATION_CHANGE(3),

  USER_LOGIN(100),
  USER_LOGIN_FAILED(101),
  USER_LOGOUT(102),
  USER_AUTH_CHANGE(103),
  USER_LOCK(104),

  DATA_CHANGE(200);

  private final int auditEventId;

  AuditEvent(int auditEventId) {
    this.auditEventId = auditEventId;
  }

  public int getId() {
    return auditEventId;
  }
}
