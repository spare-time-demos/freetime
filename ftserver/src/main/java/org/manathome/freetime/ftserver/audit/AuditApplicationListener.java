package org.manathome.freetime.ftserver.audit;

import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class AuditApplicationListener implements
        ApplicationListener<ContextRefreshedEvent> {

  @Autowired private AuditLog auditLog;

  @Override public void onApplicationEvent(ContextRefreshedEvent event) {

    auditLog.log(AuditEvent.APPLICATION_START,
            event.getApplicationContext().getApplicationName());
  }

  @PreDestroy
  public void destroy() {

    auditLog.log(AuditEvent.APPLICATION_STOP,
            "pre-destroy audit-log");
  }

}
