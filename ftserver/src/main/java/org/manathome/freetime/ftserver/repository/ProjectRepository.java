package org.manathome.freetime.ftserver.repository;

import org.manathome.freetime.ftserver.pd.FtProject;
import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

/**
 * database repository for projects.
 *
 * @author man@home
 */
public interface ProjectRepository extends CrudRepository<FtProject, Long> {

  Stream<FtProject> findByProjectLeads(FtTeamMember lead);
}
