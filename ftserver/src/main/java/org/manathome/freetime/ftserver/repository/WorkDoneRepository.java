package org.manathome.freetime.ftserver.repository;

import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.manathome.freetime.ftserver.pd.FtWorkDone;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.stream.Stream;

/** db access. */
public interface WorkDoneRepository extends CrudRepository<FtWorkDone, Long> {

  Stream<FtWorkDone> findByDateAt(LocalDate dateAt);

  @Query("SELECT w FROM FtWorkDone w WHERE "
          + "w.teamMember = :teamMember AND "
          + "w.dateAt <= :until AND "
          + "w.dateAt >= :from ")
  Stream<FtWorkDone> findWorkDoneByMember(
          @Param("teamMember") FtTeamMember teamMember,
          @Param("from") LocalDate from,
          @Param("until") LocalDate until);
}

