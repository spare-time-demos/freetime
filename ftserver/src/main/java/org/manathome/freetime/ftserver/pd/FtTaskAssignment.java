package org.manathome.freetime.ftserver.pd;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.manathome.freetime.ftserver.support.Require;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;

/** planned assignment of a team member to a given project task. */
@Entity
@Table(name = FtTaskAssignment.TABLE_NAME)
public class FtTaskAssignment {

  public static final String TABLE_NAME = "FT_TASK_ASSIGNMENT";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "team_member_id", nullable = false)
  private FtTeamMember teamMember;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "task_id", nullable = false)
  private FtTask task;

  @Column(name = "date_from", nullable = false)
  private LocalDate from;

  @Column(name = "date_until", nullable = true)
  private LocalDate until;

  /** builder. */
  public static FtTaskAssignment buildForTest(long teamMemberId, long taskId) {
    final var tm = FtTeamMember.buildForTest(teamMemberId, "dummy user", "dummy");
    final var t = FtTask.buildForTest(taskId, "dummy task");
    return new FtTaskAssignment(tm, t, LocalDate.now().minusDays(10));
  }

  /** jpa .ctor. */
  public FtTaskAssignment() {

  }

  /** create an assignment. */
  public FtTaskAssignment(final FtTeamMember member, final FtTask task, LocalDate from) {
    this.teamMember = Require.notNull(member);
    this.task = Require.notNull(task);
    this.from = Require.notNull(from);
  }

  @Schema(description = "internal unique assignment id", example = "-1")
  public Long getId() {
    return id;
  }

  @Schema(description = "assigned team member")
  public @NotNull FtTeamMember getTeamMember() {
    return teamMember;
  }

  @JsonIgnore
  public FtTask getTask() {
    return task;
  }

  public void setTask(final FtTask task) {
    this.task = task;
  }

  @Schema(description = "assignment start date", example = "2019-01-02")
  public @NotNull LocalDate getFrom() {
    return from;
  }

  @Schema(description = "optional assignment end date", example = "2021-12-31", required = false)
  public LocalDate getUntil() {
    return until;
  }

  /**
   * assignment time range.
   *
   * @param from  required from date
   * @param until optional until date
   */
  public void setAssignmentRange(final LocalDate from, final LocalDate until) {

    Require.notNull(from);
    if (until != null && from != null) {
      Require.isTrue(until.isAfter(from));
    }
    this.from = from;
    this.until = until;
  }

  @Override
  public String toString() {
    return "Assignment[id=" + id + ", teamMember=" + teamMember
            + ", task=" + task + ", from=" + from + "]";
  }

  /** may this work be added to current week by given user. */
  public boolean allowesWork(final FtWorkDone workDone) {

    return workDone != null
            && workDone.isValid()
            && workDone.getTeamMember().getId().longValue() == this.getTeamMember().getId().longValue()
            && workDone.getTask().getId().longValue() == this.getTask().getId().longValue()
            && !workDone.getDateAt().isBefore(this.from)
            && (this.until == null || !workDone.getDateAt().isAfter(this.until));
  }

}
