package org.manathome.freetime.ftserver.tracking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.manathome.freetime.ftserver.support.TechnicalApplicationException;

/** tracking data sent by ftclient spa. */
public record TrackingData(
  @JsonProperty("client_id")
  String clientId,

  @JsonProperty("user_id")
  String userId,

  @JsonProperty(value = "events", required = false)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  TrackingDataEvent[] events) {
  public String toJson() {
    try {
      return new ObjectMapper().writeValueAsString(this);
    } catch (JsonProcessingException e) {
      throw new TechnicalApplicationException("error while produce json from tracking data", e);
    }
  }
}

