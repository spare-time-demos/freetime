package org.manathome.freetime.ftserver.support;


/**
 * freetime specific runtime exception.
 */
public class TechnicalApplicationException extends RuntimeException {

  private static final long serialVersionUID = -731600236808574112L;

  public TechnicalApplicationException(String message) {
    super(message);
  }

  public TechnicalApplicationException(String message, Throwable cause) {
    super(message, cause);
  }

}
