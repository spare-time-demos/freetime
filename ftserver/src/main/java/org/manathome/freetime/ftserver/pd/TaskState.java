package org.manathome.freetime.ftserver.pd;

import io.swagger.v3.oas.annotations.media.Schema;
import org.manathome.freetime.ftserver.support.Require;

/**
 * State of a Task.
 *
 * @category db
 * @see FtTask#getState()*/
public enum TaskState {

  /** P. */
  @Schema(description = "p: planned task.")
  PLANNED("P"),

  /** A. */
  @Schema(description = "a: active, usable task state.")
  ACTIVE("A"),

  /** D. */
  @Schema(description = "d: done task, no further usage.")
  DONE("D");

  private final String stateCode;

  TaskState(final String code) {
    this.stateCode = Require.notNullOrEmptyWhitespace(code, "code");
  }

  public String getCode() {
    return stateCode;
  }
}
