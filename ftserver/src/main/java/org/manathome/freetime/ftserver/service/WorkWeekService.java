package org.manathome.freetime.ftserver.service;

import com.google.common.collect.Streams;

import io.micrometer.core.instrument.Metrics;
import jakarta.validation.constraints.NotNull;
import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.manathome.freetime.ftserver.pd.FtWorkDone;
import org.manathome.freetime.ftserver.pd.FtWorkWeek;
import org.manathome.freetime.ftserver.repository.TaskAssignmentRepository;
import org.manathome.freetime.ftserver.repository.TaskRepository;
import org.manathome.freetime.ftserver.repository.WorkDoneRepository;
import org.manathome.freetime.ftserver.support.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/** process a whole week of work for read and update. */
@Service
@Transactional
public class WorkWeekService {

  private static final Logger logger = LoggerFactory.getLogger(WorkWeekService.class);

  @Autowired
  private TaskAssignmentRepository taskAssignmentRepository;

  @Autowired
  private TaskRepository taskRepository;

  @Autowired
  private WorkDoneRepository workDoneRepository;

  /** read and build work week. */
  public @NotNull FtWorkWeek getWorkWeek(
          @NotNull final FtTeamMember teamMember,
          @NotNull final LocalDate from) {

    final var until = from.plusDays(6); // 7 days a week

    logger.trace("for user " + teamMember.getName() + " from " + from);

    final var workDone = workDoneRepository
            .findWorkDoneByMember(teamMember, from, until)
            .collect(Collectors.toList());

    final long sumWorkDone = workDone.stream().mapToLong(wd -> wd.getWorkHours()).sum();
    logger.trace("found: " + workDone.size() + " workdone entries with: " + sumWorkDone + "min");

    final var taskAssignments = taskAssignmentRepository
            .findAssignedTasksForMember(teamMember, from, until)
            .collect(Collectors.toList());

    logger.trace("found: " + taskAssignments.size() + " assignments");

    final var workWeek = new FtWorkWeek(from, until, taskAssignments, workDone);
    logger.trace("built: " + workWeek);

    Require.isTrue(sumWorkDone == workWeek.sumWorkHours(),
            "Lost Minutes for " + from + ": " + sumWorkDone + " -> "
                    + workWeek.sumWorkHours());

    Metrics.counter(
            "workweek",
            "db",
                  "read")
            .increment();

    return workWeek;
  }

  /** deconstruct and save work from week. */
  public void updateWorkWeek(
          @NotNull final FtTeamMember teamMember,
          @NotNull final FtWorkWeek workWeek) {

    Require.notNull(workWeek, "workWeek");

    final var until = workWeek.getFrom().plusDays(6); // 7 days a week

    workWeek.validate();

    // read allowed tasks for this week.
    final var allowedTaskAssignments = taskAssignmentRepository
            .findAssignedTasksForMember(teamMember, workWeek.getFrom(), until)
            .collect(Collectors.toList());

    logger.trace("for user " + teamMember.getName());

    // read current already stored work for this week.
    final Map<String, FtWorkDone> currentWorkDone = workDoneRepository
            .findWorkDoneByMember(teamMember, workWeek.getFrom(), until)
            .collect(Collectors.toMap(FtWorkDone::getCompareString, workDone -> workDone));



    // find new work to store
    final var newWorkDone = Arrays.stream(workWeek.getTaskWeeks()).flatMap(tw -> {
      logger.trace("updating new work: " + tw.toDump());
      return Streams.mapWithIndex(Arrays.stream(tw.getWorkHours()),
              (wh, index) -> new FtWorkDone(
                      teamMember,
                      taskRepository.findById(tw.getTaskId()).get(),
                      workWeek.getFrom().plusDays(index),
                      wh)
              );
    }).collect(Collectors.toMap(FtWorkDone::getCompareString, workDone -> workDone));

    // find stored work that needs to be deleted

    final List<FtWorkDone> removedWork = currentWorkDone.values().stream()
            .filter(cw -> !newWorkDone.containsKey(cw.getCompareString())).collect(Collectors.toList());

    final var deleteCounter = Metrics.counter(
            "workdone",
            "db",
            "delete");
    final var updateCounter = Metrics.counter(
            "workdone",
            "db",
            "update");
    final var insertCounter = Metrics.counter(
            "workdone",
            "db",
            "insert");

    final var workHoursCounter = Metrics.counter("workHours_added");

    // db delete
    // ---------
    removedWork.forEach(rw -> {
      logger.debug("removing old work: " + rw.toString());
      workDoneRepository.delete(rw);
      deleteCounter.increment();
    });

    // db insert
    // ---------
    final List<FtWorkDone> newWork = newWorkDone.values().stream()
            .filter(nw -> !currentWorkDone.containsKey(nw.getCompareString()))
            .filter(nw -> nw.getWorkHours() != 0).collect(Collectors.toList());

    newWork.stream().sorted(Comparator.comparing(FtWorkDone::getDateAt)).forEach(nw -> {
      logger.debug("saving new work: " + nw.toString());
      Require.isTrue(allowedTaskAssignments.stream().anyMatch(ata -> ata.allowesWork(nw)),
              "new work " + nw + " not allowed by current task assignments.");
      Require.isTrue(nw.isValid(), "new work must be valid: " + nw);
      workDoneRepository.save(nw);
      insertCounter.increment();
      workHoursCounter.increment(nw.getWorkHours());
    });

    // db update
    // ---------
    final List<FtWorkDone> changedWork = newWorkDone.values().stream()
            .filter(nw -> currentWorkDone.containsKey(nw.getCompareString()))
            .filter(nw -> currentWorkDone
                    .get(nw.getCompareString()).getWorkHours() != nw.getWorkHours())
            .map(nw -> {
              var toChange = currentWorkDone.get(nw.getCompareString());

              if (nw.getWorkHours() > toChange.getWorkHours()) {
                workHoursCounter.increment((long) nw.getWorkHours()
                        - (long) toChange.getWorkHours());
              }

              toChange.setWorkHours(nw.getWorkHours());
              return toChange;
            }).collect(Collectors.toList());

    changedWork.forEach(cw -> {
      logger.debug("update changed work: " + cw.toString());
      Require.isTrue(allowedTaskAssignments.stream().anyMatch(ata -> ata.allowesWork(cw)),
              "change work + " + cw + " not allowed by current task assignments.");
      Require.isTrue(cw.isValid(), "changed work must be valid: " + cw);
      workDoneRepository.save(cw);
      updateCounter.increment();
    });

  }

}
