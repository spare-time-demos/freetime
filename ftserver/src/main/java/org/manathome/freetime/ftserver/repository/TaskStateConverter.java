package org.manathome.freetime.ftserver.repository;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.manathome.freetime.ftserver.pd.TaskState;

import java.util.stream.Stream;

/** TaskState to Database Column Value. */
@Converter(autoApply = true)
public class TaskStateConverter implements AttributeConverter<TaskState, String> {

  @Override
  public String convertToDatabaseColumn(TaskState taskState) {
    if (taskState == null) {
      return null;
    }
    return taskState.getCode();
  }

  @Override
  public TaskState convertToEntityAttribute(String taskState) {
    if (taskState == null) {
      return null;
    }

    return Stream.of(TaskState.values())
            .filter(c -> c.getCode().equals(taskState)).findFirst()
            .orElseThrow(IllegalArgumentException::new);
  }
}

