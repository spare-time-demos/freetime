package org.manathome.freetime.ftserver.tracking;

import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

/** forward tracking information.
 *  <a href="https://www.google-analytics.com">https://www.google-analytics.com"</a>
 * stream id 4623147237
 * mess id G-DX9GZNZRVC
 **/
@Service
@SuppressWarnings("PMD.MoreThanOneLogger")
public class TrackingService {


  private static final Logger LOG = LoggerFactory.getLogger(TrackingService.class);
  private static final Logger TRACKING_LOG = LoggerFactory.getLogger("TRACKING");


  private static final String GOOGLE_MEASUREMENT_ID = "G-DX9GZNZRVC";

  @Value("${freetime.google_api_secret}")
  private String googleApiSecret;

  private final WebClient debugTrackingClient = WebClient.create("https://www.google-analytics.com/debug");

  private final WebClient googleTrackingClient = WebClient.create("https://www.google-analytics.com");

  /** production call tracking. */
  public boolean track(final @NotNull String trackingData) {

    return track(trackingData, googleTrackingClient)
            .isEmpty();
  }

  /** debug call tracking.
   * @return "" ok, error diagnoses on error */
  public String trackDebug(final @NotNull String trackingData) {

    return track(trackingData, debugTrackingClient)
            .orElse("");
  }

  /** call Google Analytics.
   * @return empty if ok.
   */
  Optional<String> track(
            final @NotNull String trackingData,
            final @NotNull WebClient webClient) {

      TRACKING_LOG.info(trackingData);

      try {
        var response = webClient
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path("/mp/collect")
                        .queryParam("measurement_id", GOOGLE_MEASUREMENT_ID)
                        .queryParam("api_secret", googleApiSecret)
                        .build()
                )
                .bodyValue(trackingData)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.USER_AGENT, "freetime-ftserver-httpClient")
                .retrieve()
                .onStatus(
                        HttpStatusCode::isError,
                        resp -> Mono.error(new Exception("tracking error." + resp.statusCode())))
                .bodyToMono(String.class)
                .block();

        if (response == null || response.length() == 0) {
          LOG.debug("tracking sent to ga4: " + trackingData);
          return Optional.empty(); // ok
        }

        LOG.debug("tracking ga4 failed with" + response + " " + trackingData);

        return Optional.of(response);

      } catch (Exception ex) {  // NOPMD supress warning
        LOG.info("tracking ga4 failed with Exception: " + trackingData + ": " + ex.getMessage());
        return Optional.of(ex.getMessage());
      }
  }

}
