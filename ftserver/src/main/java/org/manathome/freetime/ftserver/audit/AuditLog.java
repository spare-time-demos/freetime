package org.manathome.freetime.ftserver.audit;


import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/** audit log. */
@Service
public class AuditLog {

  private static final Logger AUDIT_LOG = LoggerFactory.getLogger("AUDIT");

  AuditLog() {
    AUDIT_LOG.info(AuditEvent.APPLICATION_START + ": Audit Log initialized");
  }

  public void log(
          final @NotNull AuditEvent auditEvent,
          final @NotNull String auditMessage) {

    AUDIT_LOG.info(auditEvent
                   + ":"
                   + auditMessage);
  }

}
