package org.manathome.freetime.ftserver.pd;

import io.swagger.v3.oas.annotations.media.Schema;
import org.manathome.freetime.ftserver.support.Require;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;

/**
 * public faced data.
 *
 * @category dto.
 */
@Schema(name = "FtTaskWeek", description = "one week of working hours for a given task.")
public class FtTaskWeek {

  private static final int DAYS_IN_WEEK = 7;

  private Long taskId;
  private String taskName;

  private final Integer[] workHours = new Integer[DAYS_IN_WEEK];

  private FtTaskWeek() {
  }

  /**
   * .ctor.
   */
  public FtTaskWeek(
          final LocalDate from,
          final FtTaskAssignment taskAssignment,
          final Collection<FtWorkDone> workDone) {
    this();

    Require.notNull(workDone, "workDone");
    Require.isTrue(taskAssignment != null || !workDone.isEmpty(),
            "assignment or workDone required.");

    final var task = taskAssignment == null
            ? Require.notNull(workDone, "workDone")
            .stream()
            .findFirst()
            .orElseThrow()
            .getTask()
            : taskAssignment.getTask();

    taskId = task.getId();
    taskName = task.getName();

    for (int dayIndex = 0; dayIndex < DAYS_IN_WEEK; dayIndex++) {

      final var day = from.plusDays(dayIndex);

      // task is not assigned OR task assignment does not contain given day
      final boolean isReadOnly = taskAssignment == null
              || task.getState() == TaskState.DONE
              || taskAssignment.getFrom().isAfter(day)
              || (taskAssignment.getUntil() != null && taskAssignment.getUntil().isBefore(day));

      final var calcWorkHours = workDone.stream()
              .filter(wd -> wd.getDateAt().isEqual(day)
                      && wd.getTask().getId().longValue() == taskId.longValue())
              .map(FtWorkDone::getWorkHours)
              .findFirst()
              .orElse(0);

      this.workHours[dayIndex] = calcWorkHours;
    }
  }

  @Schema(description = "task name (prose).")
  public String getTaskName() {
    return this.taskName;
  }

  @Schema(description = "task unique id")
  public Long getTaskId() {
    return this.taskId;
  }

  @Schema(description = "7 days array of working hours")
  public Integer[] getWorkHours() {
    return workHours;
  }

  /**
   * sum hours.
   */
  public int sumWorkHours() {
    return Arrays
            .stream(workHours)
            .mapToInt(wd -> wd)
            .sum(); //NOSONAR
  }

  @Override
  public String toString() {
    return "TW[task=" + taskName + ", workHours=" + sumWorkHours() + "]";
  }

  /**
   * dump values to logs etc.
   */
  public String toDump() {
    return "TW[task:" + taskId + "," + taskName + ": Mo:"
            + workHours[0] + ", Tu:"
            + workHours[1] + ", We:"
            + workHours[2] + ", Th:"
            + workHours[3]
            + ", Fr:" + workHours[4]
            + ", Sa:" + workHours[5] + ", So:"
            + workHours[6] + "]\n";
  }

  /**
   * check require.
   */
  public void validate() {
    Require.notNull(this.taskId, "taskId");
    Require.notNull(getWorkHours(), "workhours");
    Require.isTrue(getWorkHours().length == DAYS_IN_WEEK, "workhours length not " + DAYS_IN_WEEK);
    for (final var wh : this.workHours) {
      Require.notNull(wh, "worhHour");
    }
  }

}

