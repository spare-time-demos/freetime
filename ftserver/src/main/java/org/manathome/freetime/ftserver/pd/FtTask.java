package org.manathome.freetime.ftserver.pd;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import org.manathome.freetime.ftserver.support.Require;

/** a project task to be worked on. */
@Entity
@Table(name = FtTask.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(name = "UQ_TASK_NAME_PER_PROJECT",
                        columnNames = { "project_id", "name" })})
public class FtTask {

  public static final String TABLE_NAME = "FT_TASK";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  private String name;

  @NotNull
  @Column(name = "state", nullable = false)
  private TaskState state;

  @NotNull
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "project_id")
  private FtProject project;

  /** builder. */
  public static FtTask buildForTest(String name) {
    final var p = FtProject.buildForTest(-1, "dummy project");
    return new FtTask(p, name);
  }

  /** builder. */
  public static FtTask buildForTest(long id, String name) {
    final var t = buildForTest(name);
    t.setId(id);
    return t;
  }

  /** jpa required ctor. */
  protected FtTask() {
  }

  /** ctor. */
  public FtTask(final FtProject project, final String name) {
    this.project = Require.notNull(project, "project");
    this.name = Require.notNull(name);
    this.state = TaskState.PLANNED;
  }

  public Long getId() {
    return id;
  }

  private void setId(final Long id) {
    this.id = id;
  }

  @Schema(description = "unique task name within project", minLength = 1, maxLength = 100,
          type = "string", required = true, example = "first task: do something")
  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = Require.notNull(name);
  }

  public FtProject getProject() {
    return project;
  }

  public TaskState getState() {
    return state;
  }

  public void setState(final TaskState state) {
    this.state = Require.notNull(state);
  }

  @Override
  public String toString() {
    return "Task[" + id + ", " + name + ", state: " + state + "]";
  }

}

