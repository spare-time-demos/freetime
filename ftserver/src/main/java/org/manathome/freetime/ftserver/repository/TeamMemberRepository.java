package org.manathome.freetime.ftserver.repository;

import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TeamMemberRepository extends CrudRepository<FtTeamMember, Long>  {

  Optional<FtTeamMember> findByUserId(String userId);

}

