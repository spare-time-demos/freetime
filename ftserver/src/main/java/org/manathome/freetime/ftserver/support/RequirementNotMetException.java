package org.manathome.freetime.ftserver.support;

public class RequirementNotMetException extends TechnicalApplicationException {

  private static final long serialVersionUID = -731600236808570000L;

  public RequirementNotMetException(String message) {
    super(message);
  }

  public RequirementNotMetException(String message, Throwable cause) {
    super(message, cause);
  }
}
