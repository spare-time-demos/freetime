package org.manathome.freetime.ftserver.api;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * provide some root / output for browser.
 */
@OpenAPIDefinition(
        info = @Info(
                title = "freetime server OpenAPI Schema",
                description = "internal rest openapi for freetime client",
                version = "0.2"
        ),
        externalDocs = @ExternalDocumentation(url = "https://gitlab.com/spare-time-demos/freetime",
                description = "hosted on gitlab")
)

@RestController
@CrossOrigin
public class RootController {

    private static final String MSG = "freetime client\n\n an api server for ftclient";

    @GetMapping("/")
    @Operation(summary = "root", hidden = true)
    public @ResponseBody String home() {
        return MSG;
    }

    @GetMapping("/ftserver")
    @Operation(summary = "root ftserver", hidden = true)
    public @ResponseBody String ftserverhome() {
        return MSG;
    }

}
