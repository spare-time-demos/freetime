package org.manathome.freetime.ftserver.support;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Date;

@Schema(name = "Info", description = "application info")
public class InfoHolder {

  @Schema(description = "application name", example = "freetime server", requiredMode = Schema.RequiredMode.REQUIRED)
  private String name;
  @Schema(description = "current server time", requiredMode = Schema.RequiredMode.REQUIRED)
  private Date currentTime;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCurrentTime() {
    return new Date(currentTime.getTime());
  }

  public void setCurrentTime(Date currentTime) {
    this.currentTime = new Date(currentTime.getTime());
  }
}
