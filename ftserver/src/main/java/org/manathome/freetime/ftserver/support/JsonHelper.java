package org.manathome.freetime.ftserver.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public final class JsonHelper {

  private JsonHelper() {

  }

  /** serialize to json. */
  public static String asJsonString(final Object objToSerialize) {

    if (objToSerialize == null) {
      return "";
    }

    try {
      final var mapper = new ObjectMapper();
      mapper.registerModule(new JavaTimeModule());
      mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
      return mapper.writeValueAsString(objToSerialize);
    } catch (Exception e) {
      throw new TechnicalApplicationException("json conversion error: " + e.getMessage(),e);
    }
  }

}
