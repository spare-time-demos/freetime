package org.manathome.freetime.ftserver;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Tag(name = "freetime-api", description = "Published API")
@SuppressWarnings("PMD")
public class FtServerApplication {

    public static void main(String[] args) {

        SpringApplication.run(FtServerApplication.class, args);
    }

}
