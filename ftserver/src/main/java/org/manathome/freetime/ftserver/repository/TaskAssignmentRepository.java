package org.manathome.freetime.ftserver.repository;

import org.manathome.freetime.ftserver.pd.FtTaskAssignment;
import org.manathome.freetime.ftserver.pd.FtTeamMember;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.stream.Stream;

public interface TaskAssignmentRepository extends CrudRepository<FtTaskAssignment, Long> {

  Stream<FtTaskAssignment> findByTaskIdOrderById(Long taskId);

  @Query("SELECT ta FROM FtTaskAssignment ta WHERE ta.teamMember = :teamMember AND "
          + "ta.from <= :until AND (ta.until IS NULL OR ta.until >= :from)")
  @EntityGraph(attributePaths = { "task" })
  Stream<FtTaskAssignment> findAssignedTasksForMember(
          @Param("teamMember") FtTeamMember member,
          @Param("from") LocalDate from,
          @Param("until") LocalDate until);
}

