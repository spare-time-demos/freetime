package org.manathome.freetime.ftserver.api;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.manathome.freetime.ftserver.tracking.TrackingData;
import org.manathome.freetime.ftserver.tracking.TrackingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class TrackingController {

  private static final Logger LOG = LoggerFactory.getLogger(TrackingController.class);

  @Autowired
  private TrackingService trackingService;

  /**
   * tracking endpoint.
   */
  @PostMapping("ftserver/track")
  @Operation(summary = "tracking endpoint", description = "receive client side tracking information")
  @ApiResponse(responseCode = "200", description = "ok")
  @Timed
  public void track(final @RequestBody TrackingData trackingData) {

    try {
      trackingService.track(trackingData.toJson());
    } catch (Exception ex) {  // NOPMD suppress warning in PMD generic exception
      LOG.warn("error tracking", ex);
      // fail silently
    }

  }
}
