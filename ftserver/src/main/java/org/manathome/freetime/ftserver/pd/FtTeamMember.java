package org.manathome.freetime.ftserver.pd;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.manathome.freetime.ftserver.support.Require;

import java.io.Serial;
import java.io.Serializable;

/** employee or worker that leads or works on a project. */
@Entity
@Table(name = FtTeamMember.TABLE_NAME)
public class FtTeamMember implements Serializable {

  /**
   * required for caching.
   */
  @Serial
  private static final long serialVersionUID = -3338312632276995655L;

  public static final String TABLE_NAME = "FT_TEAM_MEMBER";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  private String name;

  @Column(name = "user_id", length = 25, nullable = false, unique = true)
  private String userId;

  @JsonIgnore
  @Column(name = "password", length = 25)
  private String password;

  @Column(name = "is_locked")
  private int lockedFlag;

  @Column(name = "is_active")
  private int activeFlag;

  @Column(name = "is_admin")
  private int adminRoleFlag;

  @Column(name = "is_lead")
  private int leadRoleFlag;

  /** builder. */
  public static FtTeamMember buildForTest(long id, String name, String userId) {
    return new FtTeamMember(id, name, userId);
  }

  /** jpa required ctor. */
  protected FtTeamMember() {
  }

  /** ctor. */
  public FtTeamMember(final Long id, final String userId, final String name) {
    this(userId, name);
    this.id = id;
  }

  /** ctor. */
  public FtTeamMember(final String userId, final String name) {
    this.name = Require.notNull(name, "name");
    this.userId = Require.notNull(userId, "userId");
  }

  public Long getId() {
    return id;
  }

  public String getUserId() {
    return userId;
  }

  public String getUserIdAsString() {
    return userId;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = Require.notNull(name);
  }

  @JsonIgnore
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  public boolean isLocked() {
    return lockedFlag == 1;
  }

  public void setLocked(boolean locked) {
    this.lockedFlag = locked ? 1 : 0;
  }

  public boolean isActive() {
    return activeFlag == 1;
  }

  public void setActive(boolean active) {
    this.activeFlag = active ? 1 : 0;
  }

  public boolean hasAdminRole() {
    return adminRoleFlag == 1;
  }

  public void setAdminRole(boolean isAdmin) {
    this.adminRoleFlag = isAdmin ? 1 : 0;
  }

  public boolean hasLeadRole() {
    return leadRoleFlag == 1;
  }

  public void setLeadRole(boolean isLead) {
    this.leadRoleFlag = isLead ? 1 : 0;
  }

  @Override
  public String toString() {
    return "TM[" + id + ": " + userId + ", " + name + "]";
  }

  /** current team member may log in into app. */
  public boolean userMayLogIn() {
    return isActive()
            && ! isLocked()
            && this.getUserId() != null
            && this.getPassword() != null;
  }

}

