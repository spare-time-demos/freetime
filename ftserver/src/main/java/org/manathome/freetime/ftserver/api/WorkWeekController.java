package org.manathome.freetime.ftserver.api;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotNull;
import org.manathome.freetime.ftserver.pd.FtWorkWeek;
import org.manathome.freetime.ftserver.repository.TeamMemberRepository;
import org.manathome.freetime.ftserver.service.WorkWeekService;
import org.manathome.freetime.ftserver.support.JsonHelper;
import org.manathome.freetime.ftserver.support.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/** http api for work week entry. */
@RestController
@CrossOrigin
@Timed
public class WorkWeekController {

  private static final Logger logger = LoggerFactory.getLogger(WorkWeekController.class);
  public static final String FROM_ISO_DATE_STRING = "from-iso-date-string";

  @Autowired
  private WorkWeekService workWeekService;

  @Autowired
  private TeamMemberRepository teamMemberRepository;

  /** rest. */
  @Operation(summary = "retrieving work week for given week, date format: "
          + "ftserver/workweek/YYYY-MM-DD.")
  @GetMapping(path = "ftserver/workweek/{" + FROM_ISO_DATE_STRING + "}",
          produces = MediaType.APPLICATION_JSON_VALUE)
  @Transactional
  public FtWorkWeek getWorkWeek(
          @Parameter(description = "date, start of week, format YYYY-MM-DD", example = "2019-03-11")
          @NotNull
          @PathVariable(name = FROM_ISO_DATE_STRING)
          final String fromIsoDateString) {

    logger.trace("ftserver/workweek/" + fromIsoDateString);

    var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"));

    //fake, constant user..
    final var teamMember = teamMemberRepository.findByUserId("tlead1").orElseThrow();

    return workWeekService.getWorkWeek(teamMember, from);
  }


  /** rest. */
  @Operation(summary = "update work done for given week")
  @PutMapping(path = "ftserver/workweek/{" + FROM_ISO_DATE_STRING + "}",
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @Transactional
  public Integer updateWorkWeek(
          @Parameter(description = "date, start of week, format YYYY-MM-DD", example = "2019-03-11")
          @NotNull
          @PathVariable(name = FROM_ISO_DATE_STRING)
          final String fromIsoDateString,
          @NotNull
          @RequestBody
          final FtWorkWeek workWeek) {

    logger.trace("ftserver/workweek/" + fromIsoDateString + " (put) of "
            + JsonHelper.asJsonString(workWeek));

    Require.notNullOrEmptyWhitespace(fromIsoDateString, "fromIsoDateSTring");
    Require.notNull(workWeek, "workWeek");
    Require.notNull(workWeek.getFrom(), "workWeek.from");

    Require.isTrue(LocalDate.parse(fromIsoDateString).isEqual(workWeek.getFrom()),
            "from " + fromIsoDateString + " and workWeek.from not equal");

    //fake, constant user..
    final var teamMember = teamMemberRepository.findByUserId("tlead1").orElseThrow();

    this.workWeekService.updateWorkWeek(teamMember, workWeek);

    return Integer.valueOf(workWeek.sumWorkHours());
  }

}
