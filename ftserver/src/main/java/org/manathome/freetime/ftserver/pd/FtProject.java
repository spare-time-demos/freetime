package org.manathome.freetime.ftserver.pd;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import org.manathome.freetime.ftserver.support.Require;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/** tracked project.
 * this entity is audited with envers.*/
@Entity
@Table(name = FtProject.TABLE_NAME)
public class FtProject {

  public static final String TABLE_NAME = "FT_PROJECT";
  public static final String TABLE_NAME_LEAD = "FT_PROJECT_LEAD";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  private String name;

  @Column(name = "active", nullable = false)
  private boolean active = true;

  @ManyToMany
  @JoinTable(name = TABLE_NAME_LEAD, joinColumns = @JoinColumn(name = "project_id"),
          inverseJoinColumns = @JoinColumn(name = "team_member_id"))

  private final Set<FtTeamMember> projectLeads;

  /** builder. */
  public static FtProject buildForTest(long id, String projectName) {
    final var p = new FtProject(projectName);
    p.setId(Long.valueOf(id));
    return p;
  }

  public FtProject() {
    this.projectLeads = new HashSet<>();
  }

  public FtProject(final String name) {
    this();
    this.name = Require.notNull(name, "name");
  }

  public Long getId() {
    return id;
  }

  private void setId(final Long id) {
    Require.isTrue(this.id == null, "id " + this.id + " cannot be changed on project");
    this.id = id;
  }

  public @NotNull String getName() {
    return Require.notNull(name);
  }

  public void setName(@NotNull String name) {
    this.name = Require.notNull(name, "name");
  }

  public Collection<FtTeamMember> getProjectLeads() {
    return Collections.unmodifiableSet(this.projectLeads);
  }

  public void addProjectLead(final FtTeamMember projectLead) {
    this.projectLeads.add(Require.notNull(projectLead, "projectLead"));
  }

  public void removeProjectLead(final FtTeamMember projectLead) {
    this.projectLeads.remove(projectLead);
  }

  public boolean isActive() {
    return this.active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  /** is project lead by currentUser. */
  public boolean hasLead(@NotNull final FtTeamMember currentUser) {
    return currentUser != null
            && this.projectLeads != null
            && this.getProjectLeads()
            .stream()
            .anyMatch(tm -> Require.notNull(currentUser.getId(), "currentUser.id")
                    .equals(tm.getId()));
  }

  @Override
  public String toString() {
    return "Project[" + id + ", " + name + ", active: " + active + "]";
  }

}

