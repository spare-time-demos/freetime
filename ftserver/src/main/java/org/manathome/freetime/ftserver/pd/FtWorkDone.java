package org.manathome.freetime.ftserver.pd;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import org.manathome.freetime.ftserver.support.Require;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/** jpa entity for work done. */
@Entity
@Table(name = FtWorkDone.TABLE_NAME)
@Schema(description = "work done")
public class FtWorkDone implements Serializable {

  public static final String TABLE_NAME = "FT_WORK_DONE";

  @Serial
  private static final long serialVersionUID = -3338312632276995655L;

  @Id
  @GeneratedValue(strategy = jakarta.persistence.GenerationType.IDENTITY)
  private Long id;


  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "team_member_id", nullable = false)
  private FtTeamMember teamMember;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "task_id", nullable = false)
  private FtTask task;

  @Column(name = "date_at", nullable = false, columnDefinition = "DATE")
  private LocalDate dateAt;

  @Column(name = "work_hours", nullable = false)
  private int workHours;

  /** ctor. */
  public FtWorkDone() {
    this.dateAt = LocalDate.now();
    this.workHours = 0;
  }

    /** ctor. */
  public FtWorkDone(final FtTeamMember teamMember, final FtTask task, final LocalDate dateAt) {
    this.teamMember = Require.notNull(teamMember);
    this.task = Require.notNull(task);
    this.dateAt = Require.notNull(dateAt);
    this.workHours = 0;
  }

  /** ctor. */
  public FtWorkDone(
          final FtTeamMember teamMember,
          final FtTask task,
          final LocalDate date,
          final int workHours) {
    this(teamMember, task, date);
    this.workHours = workHours;
  }


  @Schema(description = "unique id", example = "1234")
  public Long getId() {
    return id;
  }

  void setId(final Long id) {
    this.id = id;
  }

  @Schema(description = "date of work done", requiredMode = Schema.RequiredMode.REQUIRED)
  public LocalDate getDateAt() {
    return this.dateAt;
  }

  public void setDateAt(final LocalDate dateAt) {
    this.dateAt = dateAt;
  }

  @Schema(description = "working hours", example = "3", minimum = "0", maximum = "24")
  public int getWorkHours() {
    return workHours;
  }

  public void setWorkHours(final int workHours) {
    this.workHours = workHours;
  }

  @Override
  public String toString() {
    return "WD[" + id + ": " + dateAt + ", " + workHours + "]";
  }

  public FtTeamMember getTeamMember() {
    return teamMember;
  }

  public FtTask getTask() {
    return task;
  }

  @JsonIgnore
  public Long getTaskId() {
    return Require.notNull(this.task).getId();
  }

  public String getCompareString() {
    return "tm:" + this.getTeamMember().getId() + "-t" + this.getTaskId() + "-d" + this.getDateAt();
  }

  public boolean isValid() {
    return this.getTeamMember() != null
            && this.getTask() != null
            && this.getDateAt() != null
            && this.getWorkHours() >= 0;
  }

}
