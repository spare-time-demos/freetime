package org.manathome.freetime.ftserver.api;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.tracing.Span;
import io.micrometer.tracing.Tracer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.manathome.freetime.ftserver.support.InfoHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * simple api controller.
 * <p>
 * demonstrates a bare rest endpoint with logging, custom tracing and metrics.
 */
@RestController
@CrossOrigin
public class InfoController {

    private static final Logger LOG = LoggerFactory.getLogger(InfoController.class);

    private final Tracer tracer;

    private final MeterRegistry registry;

    public InfoController(final MeterRegistry registry, final Tracer tracer) {

        this.registry = registry;
        this.tracer = tracer;
    }

    /**
     * return text info.
     *
     * show some metrics, logging, tracing.
     */
    @GetMapping("ftserver/info")
    @Operation(summary = "application info", description = "get basic application information")
    @ApiResponse(responseCode = "200", description = "ok")
    @Timed
    public InfoHolder info() {

        LOG.info("ftserver/info call");

        Span newSubTracingSpan = this.tracer.nextSpan().name("info-subspan");

        try (Tracer.SpanInScope span = this.tracer.withSpan(newSubTracingSpan.start())) {

            var info = new InfoHolder();
            info.setCurrentTime(new Date());
            info.setName("freetime server");

            // metrics
            registry.counter("info-calls").increment();

            // custom tracing
            newSubTracingSpan.tag("app", info.getName());
            newSubTracingSpan.event("info-calls-incremented"); // You can log an event on a span - an event is an annotated timestamp

            // logging
            LOG.info("ftserver/info returning {}, {}", info.getName(), info.getCurrentTime());

            return info;

        } finally {
            newSubTracingSpan.end();  // Once done with the tracing remember to end the span. This will allow collecting
        }

    }
}
