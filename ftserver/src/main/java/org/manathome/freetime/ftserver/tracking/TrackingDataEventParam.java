package org.manathome.freetime.ftserver.tracking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public record TrackingDataEventParam(
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("page_location")
        String pageLocation,

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("page_title")
        String pageTitle,

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("engagement_time_msec")
        Long engagementTimeMsec,

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("session_id")
        String sessionId
        ) { }
