package org.manathome.freetime.ftserver.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** custom assertions to express preconditions or postconditions in code. */
@SuppressWarnings("PMD.ClassNamingConventions")
public final class Require {

  static final Logger logger = LoggerFactory.getLogger(Require.class);

  private Require() {
  }

  /** check not null. */
  public static <T> T notNull(final T notNullObject, final String nameOfNotNullExpression) {
    if (notNullObject == null) {
      logger.error("requirement not null violated on: " + nameOfNotNullExpression);
      throw new RequirementNotMetException("requirement not null violated on: " + nameOfNotNullExpression);
    }
    return notNullObject;
  }

  /** check not null. */
  public static <T> T notNull(final T notNullObject) {
    return notNull(notNullObject, "object not null expected");
  }

  /** allow neither null nor blank or empty string. */
  public static String notNullOrEmptyWhitespace(final String text) {
    return notNullOrEmptyWhitespace(text, "non empty text required");
  }

  /** allow neither null nor blank or empty string. */
  public static String notNullOrEmptyWhitespace(final String text, final String errorMsg) {
    String t = notNull(text, errorMsg);
    if (t.trim().length() == 0) {
      logger.error(errorMsg);
      throw new RequirementNotMetException(errorMsg);
    }
    return t;
  }

  /** require positive or 0,  >= 1 numbers. */
  public static int zeroOrPositive(int number) {
    if (number < 0) {
      logger.error("zeroOrPositive: value is negative: " + number);
      throw new RequirementNotMetException("value is negative: " + number);
    }
    return number;
  }

  public static boolean isTrue(boolean shouldBeTrue) {
    return isTrue(shouldBeTrue, "required check not true");
  }

  /** ensure true.
   * @return true
   */
  public static boolean isTrue(boolean shouldBeTrue, String msg) {

    if (!shouldBeTrue) {
      throw new RequirementNotMetException(msg);
    }
    return true;
  }

  public static Long notNullOrZero(Long number) {
    return notNullOrZero(number, null);
  }

  /** ensure number is given and not 0. */
  public static Long notNullOrZero(final Long number, final String name) {

    if (Require.notNull(number).longValue() == 0) {
      throw new RequirementNotMetException("number shall not be zero (0) or null "
              + (name == null ? "." : "for: " + name));
    }
    return number;
  }

  /** ensure id are equal.
   *
   * @return id if equal, null if both are null
   * @throws  RequirementNotMetException ids differ
   * */
  @SuppressWarnings("PMD.CyclomaticComplexity")
  public static Long sameId(// NOSONAR
                            final Long actualId,
                            final Long expectedId,
                            final String idName) {

    if (actualId == null && expectedId != null) {
      throw new RequirementNotMetException("null for " + idName + " is not expected value " + expectedId);
    } else if (expectedId == null && actualId != null) {
      throw new RequirementNotMetException(actualId + " as id for " + idName + " ist not null as expected");
    } else if (actualId == null && expectedId == null) {
      return null;
    }

    if (actualId.longValue() != expectedId.longValue()) {
      throw new RequirementNotMetException("id " + idName + " is " + actualId
              + ", has not expected value " + expectedId);
    }
    return actualId;
  }

}

