# create a container for freetime ftserver (spring boot backend)

# stage 0: compile and package the server
# ---------------------------------------

FROM eclipse-temurin:20-jdk as builder
# Set the working directory to /app inside the container
WORKDIR ftsrv
# Copy files from ftserver to image
COPY . .
# Build java server
RUN ./gradlew assemble --info

# stage 1: bake server-container
# ------------------------------

FROM eclipse-temurin:20-jdk as production
VOLUME /tmp

# non root
RUN addgroup ftgroup; adduser  --ingroup ftgroup --disabled-password ftuser
USER ftuser

# Copy built assets
COPY --from=builder /ftsrv/build/libs/ftserver-*-SNAPSHOT.jar ftserver.jar

# Expose port
EXPOSE 8080
# Run spring boot server
ENTRYPOINT ["java","-jar","/ftserver.jar"]
