# create a container for the intial db migration
# runs a ./gradlew update gradle liquibase task on docker compose up

FROM eclipse-temurin:20-jdk
LABEL org.opencontainers.image.authors="man@home"

# Set the working directory to /app inside the container
WORKDIR liquibase

# Copy required gradle and liquibase db migrations
COPY gradlew .
COPY *.gradle .
COPY gradle.properties .
COPY gradle gradle
COPY src/main/resources src/main/resources

RUN chmod +x gradlew

# execute db migration with gradle liquibase tasks
ENTRYPOINT ["./gradlew"]
CMD ["update", "status", "history"]