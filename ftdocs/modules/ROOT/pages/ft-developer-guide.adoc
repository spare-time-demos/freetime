= developer guide
:author: man@home
:toc:

== install developer environment

=== explicitly to install

* https://git-scm.com/[git] (and gitlab access)
* java jdk 17+ development kit
* https://www.jetbrains.com/de-de/idea/[intellij] 
  IDE for java server development
* https://code.visualstudio.com/[visual studio code] 
  IDE for client development and tests

* https://nodejs.org/en/[node.js] + npm (best via lvm) for client and e2e tests
** choco upgrade nvm 
** nvm install 18.14.2
** nvm use 18.14.2
** nvm current

* https://www.docker.com/products/docker-desktop/[docker] runtime environment


* add postgres localhost line into hosts file (windows/system/drivers/etc)
* add grafana agent to hosts file

.hosts
[source, bash]
----
127.0.0.1 postgresql postgres freetime-grafana-agent freetime
----

=== implicit added

* https://gradle.org[gradle] java build tool
* https://reactjs.org/[react] SPA Framework and 
* https://www.typescriptlang.org/[typescript]
* https://playwright.dev/[playwright] e2e tests


== run and develop locally

=== local dev

* start and init https://www.postgresql.org/[postgreSQL] database
** inside a docker container
** base db structure created with migrations by 
   https://www.liquibase.org/[liquibase]

.init database
[source, bash]
----
docker pull postgres
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=freetime -d -p 5432:5432 postgres
./gradlew dropAll update
----

* run spring boot server (tests or server) on Port 8080

.test and run java server
[source, bash]
----
./gradlew test
./gradlew runBoot
----

the dev server is running as: http://localhost:8080/ftserver/info

.update java libraries (BOM changes in build.gradle need new gradle.lockfile)
[source, bash]
----
 ./gradlew dependencies --write-locks
----

* update api description and client 
** generate schema from java server
** generate client proxies from schema

.update client api stubs from server
[source, bash]
----
ftserver> ./gradlew classes generateOpenApiDocs openApiGenerate copyApi
ftclient> npm run api:generate
----

* run typescript react client in a node dev server on port 3000

.test and run react client dev mode
[source, bash]
----
./npm test
./npm start
----

.refresh translations
[source, bash]
----
./npm run i18n:extract
./npm run i18n:compile
----

* open app at http://localhost:3000[]

* check qa

* see sonarcloud below

* run e2e tests built with https://playwright.dev/[playwright]

[source, bash]
----
npm init playwright@latest
npx playwright test --project chromium-local --headed --trace on
npx playwright show-report
----

* generate antora documentation locally (into /public folder)

.generate documentation site
[source, bash]
----
ftdocs> npm run copy-files
ftdocs> npm run build
----

a hidden option: generate a slides/presentation with asciidoc:

.generate presentation slides
[source, bash]
----
.\gradlew asciidoctorRevealJs
----

=== build single container locally

NOTE: docker compose steps a recommended.

[source, bash]
----
ftclient> docker --debug build -f deploy/ftclient.dockerfile -t freetime-ftclient .
ftserver> docker --debug build -f deploy/ftserver.dockerfile  -t freetime-ftserver .
----

run individual freetime containers with docker 

[source, bash]
----
docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 postgres
docker run -it --rm --name freetime-ftserver -p 8080:8080 freetime-ftserver
docker run -it --rm --name freetime-ftclient -p 1337:80 freetime-ftclient
----

=== build and run test environment locally with docker compose

or build and run the complete package with docker compose.
`build` recreates the containers, `up` will start all containers.

[source, bash]
----
docker-compose -f deploy/docker-compose.test.yaml build

echo run the app
docker-compose -f deploy/docker-compose.test.yaml up

echo run the app with monitoring infrastructure
docker-compose -f deploy/docker-compose.test.yaml --profile internal-monitoring up
----

.run e2e like the ci pipeline
[source, bash]
----
echo init db 
docker-compose -f deploy/docker-compose.test.yaml up freetime-ftserver-dbinit --abort-on-container-exit 

echo run e2e
docker-compose -f deploy/docker-compose.test.yaml --profile e2e up --abort-on-container-exit --exit-code-from freetime-playwright --attach freetime-playwright
----


* app is running at http://localhost[]
** all behind traefik

more docker compose commands:

[source, bash]
----
docker-compose -f deploy/docker-compose.test.yaml logs freetime-ftserver
docker-compose -f deploy/docker-compose.test.yaml ps
docker-compose -f deploy/docker-compose.test.yaml down
docker-compose -f deploy/docker-compose.test.yaml rm

echo reexecute db migrations ..
docker-compose -f deploy/docker-compose.test.yaml up --build freetime-ftserver-dbinit

echo run specific container only:
docker-compose -f deploy/docker-compose.test.yaml run freetime-grafana-agent
----


=== run load tests with k6

* https://k6.io/
* https://github.com/benc-uk/k6-reporter

.. using k6
[source, bash]
----
har-to-k6  .\call-week-entry-page.har -o .\call-week-entry-page.raw.js 

k6 run -e BASE_URL=http://localhost --out csv=reports/k6.call-entry-page.test-results.csv .\call-week-entry-page.js

k6 login cloud -t f816....dde
k6 cloud -e BASE_URL=http://localhost .\call-week-entry-page.js

----

=== run load test with gatling

* https://gatling.io/

.. using gatling
[source, bash]
----
 ./gradlew gatlingRun -DbaseUrl=http://localhost
----



=== dependenciy vulnerability scanning

* https://aquasecurity.github.io/trivy/v0.38/docs/

.. using trivy
[source, bash]
----
docker pull aquasec/trivy:0.38.3

echo check images
docker run -v /var/run/docker.sock:/var/run/docker.sock -v D:/_data/projects/freetime/.trivy-cache:/root/.cache/ aquasec/trivy:0.38.3 image traefik

docker run -v /var/run/docker.sock:/var/run/docker.sock -v D:/_data/projects/freetime/.trivy-cache:/root/.cache/ aquasec/trivy:0.38.3 image postgres

docker run -v /var/run/docker.sock:/var/run/docker.sock -v D:/_data/projects/freetime/.trivy-cache:/root/.cache/ aquasec/trivy:0.38.3 image freetime-ftclient

docker run -v /var/run/docker.sock:/var/run/docker.sock -v D:/_data/projects/freetime/.trivy-cache:/root/.cache/ aquasec/trivy:0.38.3 image freetime-ftserver --timeout 25m --scanners vuln --quiet

echo check images check filesystem
docker run -v D:/_data/projects/freetime/ftclient:/ftclient -v D:/_data/projects/freetime/.trivy-cache:/root/.cache/ aquasec/trivy:0.38.3 fs --scanners vuln --dependency-tree /ftclient

./gradlew dependencies --write-locks

docker run -v D:/_data/projects/freetime/ftserver:/ftserver -v D:/_data/projects/freetime/.trivy-cache:/root/.cache/ aquasec/trivy:0.38.3 fs --scanners vuln --timeout 25m /ftserver

----

== planning

default: with gitlab issues.

.gitlab issues to work on
image::developer-guide/developer-guide.gitlab-issues.png[gitlab issues screenshot]




== development process

* use a ready issue
* change issues state / label to "doing"
* create merge request (draft) with new branch
* work on branch
** code
** test
** ci pipeline on branch is working
* merge branch and close issue

=== definition of ready (DoR)

* issue is opened _hard criteria_
* functional and non functional requirements are defined
* issue must be prioritized
* issue should be estimated


include::ft-definition-of-done.adoc[leveloffset=+2]

== CI build pipeline

the project is build with a gitlab ci pipeline,

=== execution results

most recent executions can be found on:

* https://gitlab.com/spare-time-demos/freetime/-/pipelines

.gitlab ci pipeline
image::developer-guide/developer-guide.gitlab-ci-pipeline.png[gitlab ci pipeline screenshot]

=== spring boot java build

.ftserver/build.gradle
[%collapsible]
====
[source, groovy]
----
include::example$/build.gradle[]
----
====

=== react typescript build


=== included: code quality analysis

several code metrics are calculated and published 
to https://sonarcloud.io/explore/projects[sonarcloud].

* static code smells
* test code coverage
* code complexity metrics

results for freetime:

* sonarcloud dashboard: https://sonarcloud.io/organizations/spare-time-demos/projects[]
** login with my gitlab account

data publishing:

* via CI Pipeline during the build steps 
* has SONAR_ Key for API Token configured
* uses a gitlab api token

.sonarcloud overview
image::developer-guide/developer-guide.sonarcloud.png[sonarcloud screenshot]

.sonarcloud code coverage
image::developer-guide/developer-guide.sonarcloud.codecoverage.png[sonarcloud screenshot]

.sonarcloud codesmells
image::developer-guide/developer-guide.sonarcloud.codesmell.png[sonarcloud screenshot]


or you can check most stats locally by running the separate tools with:

. qa build steps available locally
[source, bash]
----

echo client
npm audit
npm test
npm run lint

echo server
gradlew test jacocoTestReport pmdMain checkstyleMain spotBugsMain
gradlew dependencyCheckAnalyze
----

see:

* https://www.jacoco.org/jacoco/trunk/index.html[jacoco] code coverage
* https://spotbugs.github.io[spotBugs] static code analyzer
* https://pmd.github.io/[PMD] static code analyzer

=== included: security scan

aside of an OWASP dependency scan added to the gradle build as 
xref:http://jeremylong.github.io/DependencyCheck/dependency-check-gradle/[owasp gradle plugin], used with:

.run dependency check java
[source, bash]
----
./gradlew dependencyCheckAnalyze
----

the cloud service xref:https://snyk.io/[snyk.io] is connected with gitlab to provide
security scans against the repo.

* https://app.snyk.io/org/man-at-home[snyk.io]
** via CI Pipeline, snyk uses a gitlab api token

.snyk
image::developer-guide/developer-guide.snyk.png[snyk screenshot]

=== included: project site generated with antora

* https://docs.gitlab.com/ee/user/project/pages/[gitlab pages] generated by the 
  ci pipeline with https://antora.org/[antora]
* antora is a static site generator specialized for it documentation and uses 
* plain docs in https://asciidoctor.org/[asciidoc] format

the current result can be found on:

* see: https://spare-time-demos.gitlab.io/freetime

.gitlab pages
image::developer-guide/developer-guide.gitlab-pages.png[gitlab pages generated by antora screenshot]

.antora site configuration
[%collapsible]
====

.antora configuration
[source, yaml]
----
include::example$antora-playbook.yml[]
----

====

==== complete pipeline

the complete https://docs.gitlab.com/ee/ci/[gitlab ci] pipeline is defined as:

.gitlab-ci.yml pipeline code
[%collapsible]
====

[source, yaml]
----
please view on gitlab
----

====

==== local gitlab-runner

run the ci pipeline locally on premise with a local gitlab-runner.

* install docker
* download gitlab runner
* create token for group
* register and install runner
** docker runner

* edit runner configuration (config.toml)
** privileged docker runner (privileged = true)
** longer timeout services (wait_for_services_timeout = 120)

* restart runner


.config.toml for local gitlab-runner (outdated)
[%collapsible]
====

[source, yaml]
----
concurrent = 2
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 2100

[[runners]]
  name = "localrunner-manathome"
  url = "https://gitlab.com/"
  id = 21279803
  token = "gq-here-is-a-token"
  token_obtained_at = 2023-02-20T17:26:50Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "docker:23.0-dind"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/certs/client", "/cache"]
    wait_for_services_timeout = 240
    shm_size = 0
----
====


.install gitlab-runner
[source, powershell]
----
gitlab-runner-windows-amd64.exe register
gitlab-runner-windows-amd64.exe install
gitlab-runner-windows-amd64.exe restart
----

.gitlab-runner registered
image::developer-guide/developer-guilde.gitlab-runner-registered.png[gitlab runner]

.gitlab-runner run
image::developer-guide/developer-guilde.gitlab-runner-run.png[gitlab runner]

== more to read


* https://docs.docker.com/compose/compose-file/
* https://dev.to/knowankit/setup-eslint-and-prettier-in-react-app-357b
* https://medium.com/devops-with-valentine/setup-gitlab-ci-runner-with-docker-executor-on-windows-10-11-c58dafba9191
* https://gitlab.com/spare-time-demos/junit-asciidoc-reporter


== attic 

phased out parts and experiments

=== clickup integration

** https://sharing.clickup.com/2486934/b/h/4-2558211-2/bd222762d93214f

.clickup issues board
image::developer-guide/developer-guide.clickup.png[clickup board screenshot]

