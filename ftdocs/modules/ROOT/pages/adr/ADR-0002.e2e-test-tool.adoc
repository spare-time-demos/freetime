= ADR: e2e test tool
:tag: adr

STATE: accepted 

== context

Significant investment shall be poured into e2e automated tests.
We need a save and compfortable to use technical basis.

== decision

We will use _playwright_ with tests written in typescript.

== consequences

* tests in typescript
* dependency on microsoft as main contributer

== considered alternatives

* selenium: less ease of use
* cyphress: dependent on small producer, several technological limits
