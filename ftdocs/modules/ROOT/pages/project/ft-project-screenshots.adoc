= screenshots

== freetime

=== week entry page

.week entry page
image::fttest/playwright-test-results/week-entry-page.png[screenshot]

.week entry page DE Version
image::fttest/playwright-test-results/week-entry-page.de.png[screenshot]

.week entry page EN Version
image::fttest/playwright-test-results/week-entry-page.en.png[screenshot]

== other

.playwright homepage
image::fttest/playwright-test-results/playwright.dev.screenshot.png[screenshot]
