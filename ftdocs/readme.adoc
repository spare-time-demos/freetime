= readme freetime ftdocs

freetime documentation. A static site built at the end of the ci pipeline with
documentation, build and qa reports etc.

== preview

* https://spare-time-demos.gitlab.io/freetime/

== dev workspace

* install antora: https://docs.antora.org/antora/latest/install/install-antora/
* run antora 

.build site command
----
npm run build
----

